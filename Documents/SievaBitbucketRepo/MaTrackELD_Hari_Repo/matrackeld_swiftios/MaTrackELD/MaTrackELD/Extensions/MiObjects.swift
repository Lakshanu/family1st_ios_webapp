
//
//  MiObjects.swift
//  MaTrack
//
//  Created by Midhun on 3/13/18.
//  Copyright © 2018 sievanetworks. All rights reserved.
//

import Foundation
import UIKit




class BounceButton: UIButton {
    
    @IBInspectable var baseColor: String = "2"{
        didSet {
            
            
            
            
            if baseColor == "6" {
                layer.masksToBounds = false
                layer.shadowColor = UIColor(hex: "A9A9A9").cgColor  //gray perfect for white background
//                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOpacity = 0.4
                layer.shadowOffset = CGSize(width: 1, height: 2)
                layer.shadowRadius = 3
                self.backgroundColor = UIColor(hex: "FECF00") //C1C3F3  //B6A1FF
            }
            else {
                layer.masksToBounds = false
                //layer.shadowColor = UIColor(hex: "A9A9A9").cgColor  //gray perfect for white background
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOpacity = 0.4
                layer.shadowOffset = CGSize(width: 1, height: 2)
                layer.shadowRadius = 3
            }
            
            //layer.borderColor = borderColor.CGColor
            if baseColor == "1" {
               // self.backgroundColor = UIColor.MiTheme.BtnColorPrimary
                self.setTitleColor(UIColor.white, for: .normal)
                
                self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
                //                self.titleLabel?.font = UIFont(name: "Avenir-Light", size: 15.0)!
                //                self.titleLabel?.font = UIFont.italicSystemFont(ofSize: 15)
                //                self.titleLabel?.font = UIFont.systemFont(ofSize: 17)
                //                self.titleLabel?.font = self.titleLabel?.font.withSize(20)
                //
                
            }
            else if baseColor == "2" {
                //self.backgroundColor = UIColor.MiTheme.BtnColorSecond
                self.setTitleColor(UIColor.black, for: .normal)
            }
            else if baseColor == "3" {
                self.backgroundColor = UIColor.black
                self.setTitleColor(UIColor.white, for: .normal)
                self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            }
            else if baseColor == "4" {
//                  self.imageView?.contentMode = .scaleAspectFit
//                imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5) //(bounds.width - 35)
//                titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0) //(imageView?.frame.width)!
                
            }
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //self.layer.cornerRadius = 28.5
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //self.layer.cornerRadius = 28.5
        
    }
    
    // Add some animations on button click
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        // Scale up the button
        self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 6, options: .allowUserInteraction, animations: {
            // Reset the sizes to defaults
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}


// Custom Title Label
class titleLabel: UILabel {
    
    @IBInspectable var baseColor: String = "1"{
        didSet {
            if baseColor == "1" {
                //self.textColor = UIColor.MiTheme.textFieldTitlePrimary
                self.backgroundColor = UIColor.red
            }
            else if baseColor == "2" {
                //self.textColor = UIColor.MiTheme.textFieldTitleSecond
                self.backgroundColor = UIColor.red
            }
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class CustomTextViewWithPlaceHolder: UITextView {
    
    
    //MARK: - Properties
    @IBInspectable var placeholder: String?
    @IBInspectable var placeholderColor: UIColor?
    var placeholderLabel: UILabel?
    
    
    //MARK: - Initializers
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Use Interface Builder User Defined Runtime Attributes to set
        // placeholder and placeholderColor in Interface Builder.
        if self.placeholder == nil {
            self.placeholder = ""
        }
        
        if self.placeholderColor == nil {
            self.placeholderColor = UIColor.black
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(textChanged(_:)), name: UITextView.textDidChangeNotification, object: nil)
        
    }
    
    @objc func textChanged(_ notification: Notification) -> Void {
        if self.placeholder?.count == 0 {
            return
        }
        
        UIView.animate(withDuration: 0.25) {
            if self.text.count == 0 {
                self.viewWithTag(999)?.alpha = 1
            }
            else {
                self.viewWithTag(999)?.alpha = 0
            }
        }
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if (self.placeholder?.count ?? 0) > 0 {
            if placeholderLabel == nil {
                placeholderLabel = UILabel.init()
                placeholderLabel?.lineBreakMode = .byWordWrapping
                placeholderLabel?.numberOfLines = 0
                placeholderLabel?.font = self.font
                placeholderLabel?.backgroundColor = self.backgroundColor
                placeholderLabel?.textColor = self.placeholderColor
                placeholderLabel?.alpha = 0
                placeholderLabel?.tag = 999
                self.addSubview(placeholderLabel!)
                
                placeholderLabel?.translatesAutoresizingMaskIntoConstraints = false
                placeholderLabel?.topAnchor.constraint(equalTo: self.topAnchor, constant: 7).isActive = true
                placeholderLabel?.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 4).isActive = true
                placeholderLabel?.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
                placeholderLabel?.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
            }
            
            placeholderLabel?.text = self.placeholder
            placeholderLabel?.sizeToFit()
            self.sendSubviewToBack(self.placeholderLabel!)
        }
        
        if self.text.count == 0 && (self.placeholder?.count ?? 0) > 0 {
            self.viewWithTag(999)?.alpha = 1
        }
    }
}


class underLineLbl: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        //        self.layer.cornerRadius = self.bounds.width/2
        //        self.clipsToBounds = true
        //        self.textColor = UIColor.MiTheme.BtnColorPrimary
        //        self.setProperties(borderWidth: 1.0, borderColor:UIColor.black)
        //self.backgroundColor = UIColor.MiTheme.underLineLbl
    }
    func setProperties(borderWidth: Float, borderColor: UIColor) {
        self.layer.borderWidth = CGFloat(borderWidth)
        self.layer.borderColor = borderColor.cgColor
    }
}
