//
//  LogCustomView.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/21/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class LogCustomView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        let view = Bundle.main.loadNibNamed("LogCustomView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
}
