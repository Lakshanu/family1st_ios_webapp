
import UIKit

protocol HeaderDelegate {
    func callHeader(idx: Int)
}

class HeaderView: UIView {
    
    var secIndex: Int?
    var delegate: HeaderDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(btn)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    lazy var btn: UIButton = {
        let btn = UIButton(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: 60))
        btn.bottomLineColor = .darkGray
        btn.bottomLineWidth = 1.0
        
        btn.addTarget(self, action: #selector(onClickHeaderView), for: .touchUpInside)
        return btn
        }()
        
        
    @objc func onClickHeaderView() {
        if let idx = secIndex {
            delegate?.callHeader(idx: idx)
        }
    }

}
