//
//  TrailerListViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/28/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class TrailerListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "TrailerListTableViewCell", bundle: nil), forCellReuseIdentifier: "TrailerListTableViewCell")
        tableView.rowHeight = 50
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear

    }
    
    
    @IBAction func backbtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Trailer_Shipping_Doc_ViewController") as! Trailer_Shipping_Doc_ViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func addbtn(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func camerabtn(_ sender: UIBarButtonItem) {
    }
    
}


//TableView
extension TrailerListViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrailerListTableViewCell", for: indexPath) as! TrailerListTableViewCell
        
        if(indexPath.section == 0) {
            cell.srnoLbl.text = "Sr.No."
            cell.trailerLbl.text = "Trailer"
            cell.deleteLbl.text = "Delete"
        }
        else {
                cell.srnoLbl.text = "\(indexPath.section)"
                cell.trailerLbl.text = ""
                let image: UIImage = UIImage(named: "remove")!
                var bgImage: UIImageView?
                bgImage = UIImageView(image: image)
                bgImage!.frame = CGRect(x: 60,y: 15,width: 20,height: 20)
                cell.deleteLbl.addSubview(bgImage!)
                cell.deleteLbl.text = ""
        }
        
        return cell
        
    }
    
    

}
