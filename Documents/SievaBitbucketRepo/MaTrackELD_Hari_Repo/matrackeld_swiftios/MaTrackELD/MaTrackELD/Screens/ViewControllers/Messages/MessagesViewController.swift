//
//  MessagesViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/21/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var warningSwitch: UISwitch!
    @IBOutlet var violationSwitch: UISwitch!
    @IBOutlet var tableView: UITableView!
    
    var cancelButton = getBarButton("Cancel")
    var doneButton = getBarButton("Done")
    var selectedStartDateVal = Date(timeInterval: 0, since: Date())
    var dateString = String()
    var selectDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yyyy"
        let dateString = df.string(from: date)
        dateLbl.text = dateString
    }
    
    @IBAction func warningSwitch(_ sender: UISwitch) {
        if(warningSwitch.isOn) {
            tableView.isHidden = false
            tableView.delegate = self
            tableView.dataSource = self
            tableView.register(UINib.init(nibName: "MessagesTableViewCell", bundle: nil), forCellReuseIdentifier: "MessagesTableViewCell")
            tableView.rowHeight = 80
            tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
            tableView.separatorColor = .clear
        }
        else {
            tableView.isHidden = true
        }
        

    }
    
    @IBAction func violationSwitch(_ sender: UISwitch) {
    }
    
    @IBAction func refreshBtnAction(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func calendarBtnAction(_ sender: UIBarButtonItem) {
        dateSelection()
    }
    
    @IBAction func cameraBtnAction(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func dateSelection() {
        let datePicker = ActionSheetDatePicker(title: dateString, datePickerMode: UIDatePicker.Mode.date, selectedDate:selectedStartDateVal , doneBlock: {
                   picker, value, index in
                   
                   if let selectedDate = value as? Date {
                       self.selectDate = selectedDate
                       let dateForma = DateFormatter()
                       dateForma.dateFormat = "yyyy/MM/dd"
                       dateForma.locale = Locale(identifier: "en_US_POSIX")
                       self.dateLbl.text = selectedDate.toString(dateFormat: "MM/dd/yyyy")
                       self.selectedStartDateVal = (value as? Date)!
                   }
            
                   print("value = \(value!)")
                   print("index = \(String(describing: index))")
                   print("picker = \(String(describing: picker))")
                   return
        }, cancel: { ActionStringCancelBlock in return }, origin: view?.superview)
               //datePicker?.locale = Locale(identifier: "us")
               datePicker?.locale = Locale(identifier: "en_US_POSIX")
               datePicker?.maximumDate = Date(timeInterval: 0, since: Date())
               datePicker!.setCancelButton(cancelButton)
               datePicker!.setDoneButton(doneButton)
               datePicker?.show()
               
    }

}



//Table view
extension MessagesViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesTableViewCell", for: indexPath) as! MessagesTableViewCell
        return cell
        
    }

}
