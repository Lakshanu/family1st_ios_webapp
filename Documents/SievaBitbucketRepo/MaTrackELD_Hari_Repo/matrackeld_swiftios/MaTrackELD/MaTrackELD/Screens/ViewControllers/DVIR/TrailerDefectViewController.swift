//
//  TrailerDefectViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class TrailerDefectViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

var defectList = ["Brakes Connections","Brakes","Coupling Devices","Coupling(Kin)Ping","Doors","Hitch","Landing Gear","Lights-All","Reflectors/Reflective Tape","Roof","Suspension System","Tarpauline","Tires","Wheels and Rims","Trailer Other"]
    
@IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "VehicleDefectsTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleDefectsTableViewCell")
        tableView.rowHeight = 60
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
       
    }
    
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
}


//TableView
extension TrailerDefectViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return defectList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleDefectsTableViewCell", for: indexPath) as! VehicleDefectsTableViewCell
        cell.lblNo.text = "\(indexPath.row + 1) - "
        cell.defectsLbl.text = defectList[indexPath.row]
        return cell
        
    }
    
    

}
