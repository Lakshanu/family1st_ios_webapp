//
//  LoginViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("LoginViewController")
        
    }
    
    @IBAction func loginBtnClick(_ sender: UIButton) {
        
        print("loginBtnClick")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainLoginViewController") as! MainLoginViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
   
    
}
