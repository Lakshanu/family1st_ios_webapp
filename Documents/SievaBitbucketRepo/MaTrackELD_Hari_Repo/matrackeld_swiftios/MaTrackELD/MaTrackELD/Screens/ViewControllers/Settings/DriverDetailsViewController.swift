//
//  DriverDetailsViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DriverDetailsViewController: UIViewController {

    
    @IBOutlet var firstname: UITextField!
    @IBOutlet var lastname: UITextField!
    @IBOutlet var driverid: UITextField!
    @IBOutlet var driverLiceNo: UITextField!
    @IBOutlet var driverLicState: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var phoneNo: UITextField!
    @IBOutlet var firstname_string: UILabel!
    @IBOutlet var lastname_string: UILabel!
    @IBOutlet var licno_string: UILabel!
    @IBOutlet var exemptDriver: UIButton!
    
    var exemptStatus = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func exemptBtn(_ sender: UIButton) {
        if (exemptStatus == false) {
            exemptDriver.setImage(UIImage(named: "tickdump"), for: .normal)
            exemptStatus = true
        }
        else {
            exemptDriver.setImage(UIImage(named: "untickdump"), for: .normal)
            exemptStatus = false
        }
    }
    
    @IBAction func saveBtn(_ sender: UIBarButtonItem) {
    }
    
    
}
