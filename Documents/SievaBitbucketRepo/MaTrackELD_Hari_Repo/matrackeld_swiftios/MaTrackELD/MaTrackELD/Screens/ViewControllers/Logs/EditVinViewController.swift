//
//  EditVinViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/29/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class EditVinViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "EditTableViewCell", bundle: nil), forCellReuseIdentifier: "EditTableViewCell")
        tableView.rowHeight = 1000
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
        
        if(from_vin == true) {
            navigationItem.title = "Edit Vin"
        }
        else if(from_trailer == true) {
            navigationItem.title = "Edit Trailer"
        }
        else {
            navigationItem.title = "Edit Shopping Document"
        }
        
    }

    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        from_vin = false
        from_trailer = false
        from_shipping = false
        dismiss(animated: true, completion: nil)
    }
    
}



//TableView
extension EditVinViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditTableViewCell", for: indexPath) as! EditTableViewCell
        return cell
        
    }
    
    

}
