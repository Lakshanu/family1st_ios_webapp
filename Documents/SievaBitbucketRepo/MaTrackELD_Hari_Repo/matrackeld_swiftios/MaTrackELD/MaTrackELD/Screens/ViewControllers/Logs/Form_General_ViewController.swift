//
//  Form_General_ViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/25/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class Form_General_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var generalBtn: UIButton!
    @IBOutlet var carrierBtn: UIButton!
    @IBOutlet var otherBtn: UIButton!
    
    var generalStatus = Bool()
    var carrierStatus = Bool()
    var otherStatus = Bool()
    
    var cellReturn = UITableViewCell()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 1000
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
        
        if(from_general == true) {
            tableView.register(UINib.init(nibName: "Form_GeneralTableViewCell", bundle: nil), forCellReuseIdentifier: "Form_GeneralTableViewCell")
            generalBtn.borderColor = .orange
            generalBtn.setTitleColor(.orange, for: .normal)
            //cell.
            
        }
            
        else if(from_carrier == true) {
            tableView.register(UINib.init(nibName: "Form_CarrierTableViewCell", bundle: nil), forCellReuseIdentifier: "Form_CarrierTableViewCell")
            carrierBtn.borderColor = .orange
            carrierBtn.setTitleColor(.orange, for: .normal)
        }
            
        else {
            tableView.register(UINib.init(nibName: "Form_OtherTableViewCell", bundle: nil), forCellReuseIdentifier: "Form_OtherTableViewCell")
            otherBtn.borderColor = .orange
            otherBtn.setTitleColor(.orange, for: .normal)
        }
              
        NotificationCenter.default.addObserver(self, selector: #selector(Form_General_ViewController.ChangeCustomTable), name: NSNotification.Name(rawValue: "edit_vin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Form_General_ViewController.ChangeCustomTable), name: NSNotification.Name(rawValue: "edit_trailer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Form_General_ViewController.ChangeCustomTable), name: NSNotification.Name(rawValue: "edit_shipping"), object: nil)
        
    }
    
    @objc func ChangeCustomTable(){
        print("ChangeCustomTable")
        to_EditVinView()
        
    }
    
    @IBAction func generalBtnAction(_ sender: UIButton) {
       
        navigationItem.title = "General Details"
        generalBtn.borderColor = .orange
        generalBtn.setTitleColor(.orange, for: .normal)
        carrierBtn.borderColor = .systemGreen
        carrierBtn.setTitleColor(.systemGreen, for: .normal)
        otherBtn.borderColor = .systemGreen
        otherBtn.setTitleColor(.systemGreen, for: .normal)
        
        generalStatus = true
        carrierStatus = false
        otherStatus = false
        
        tableView.register(UINib.init(nibName: "Form_GeneralTableViewCell", bundle: nil), forCellReuseIdentifier: "Form_GeneralTableViewCell")
        tableView.rowHeight = 1000
        tableView.reloadData()
    }
    
    @IBAction func carrierBtnAction(_ sender: UIButton) {
        
        navigationItem.title = "Carrier Details"
        carrierBtn.borderColor = .orange
        carrierBtn.setTitleColor(.orange, for: .normal)
        generalBtn.borderColor = .systemGreen
        generalBtn.setTitleColor(.systemGreen, for: .normal)
        otherBtn.borderColor = .systemGreen
        otherBtn.setTitleColor(.systemGreen, for: .normal)
        
        generalStatus = false
        carrierStatus = true
        otherStatus = false
        
        tableView.register(UINib.init(nibName: "Form_CarrierTableViewCell", bundle: nil), forCellReuseIdentifier: "Form_CarrierTableViewCell")
        tableView.rowHeight = 1000
        tableView.reloadData()
    }
    
    @IBAction func otherBtnAction(_ sender: UIButton) {
        
        navigationItem.title = "Other Details"
        otherBtn.borderColor = .orange
        otherBtn.setTitleColor(.orange, for: .normal)
        generalBtn.borderColor = .systemGreen
        generalBtn.setTitleColor(.systemGreen, for: .normal)
        carrierBtn.borderColor = .systemGreen
        carrierBtn.setTitleColor(.systemGreen, for: .normal)
        
        generalStatus = false
        carrierStatus = false
        otherStatus = true
        
        tableView.register(UINib.init(nibName: "Form_OtherTableViewCell", bundle: nil), forCellReuseIdentifier: "Form_OtherTableViewCell")
        tableView.rowHeight = 1000
        tableView.reloadData()
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        from_formdetails = true
        dismiss(animated: true, completion: nil)
    }
    
    func to_EditVinView() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditVinViewController") as! EditVinViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
}


//TableView
extension Form_General_ViewController {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (generalStatus == true) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Form_GeneralTableViewCell", for: indexPath) as! Form_GeneralTableViewCell
            cellReturn = cell
        }
        else if (carrierStatus == true) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Form_CarrierTableViewCell", for: indexPath) as! Form_CarrierTableViewCell
            cellReturn = cell
        }
        else if (otherStatus == true) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Form_OtherTableViewCell", for: indexPath) as! Form_OtherTableViewCell
            cellReturn = cell
        }
        else {
            if(from_general == true) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Form_GeneralTableViewCell", for: indexPath) as! Form_GeneralTableViewCell
                cellReturn = cell
                from_general = false
            }
            else if(from_carrier == true) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Form_CarrierTableViewCell", for: indexPath) as! Form_CarrierTableViewCell
                cellReturn = cell
                from_carrier = false
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Form_OtherTableViewCell", for: indexPath) as! Form_OtherTableViewCell
                cellReturn = cell
                from_other = false
            }
        }
        return cellReturn
        
        
    }

}

            

