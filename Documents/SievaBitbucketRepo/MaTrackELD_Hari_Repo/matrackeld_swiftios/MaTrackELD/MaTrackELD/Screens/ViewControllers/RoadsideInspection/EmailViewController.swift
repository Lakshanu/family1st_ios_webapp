//
//  EmailViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/30/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class EmailViewController: UIViewController {

    
    @IBOutlet var toaddress: UITextField!
    @IBOutlet var ccaddress: UITextField!
    @IBOutlet var btn8: UIButton!
    @IBOutlet var btn14: UIButton!
    @IBOutlet var btn7: UIButton!
    @IBOutlet var message: UITextView!
    @IBOutlet var dvirbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func send(_ sender: UIBarButtonItem) {
    
    }
    
    @IBAction func btn7(_ sender: UIButton) {
        if(!btn7.isSelected) {
            btn7.imageView?.image = UIImage(named: "btnON")
            btn7.isSelected = true
            btn8.imageView?.image = UIImage(named: "btnOff")
            btn8.isSelected = false
            btn14.imageView?.image = UIImage(named: "btnOff")
            btn14.isSelected = false
        }
        else {
            btn7.imageView?.image = UIImage(named: "btnOff")
            btn7.isSelected = false
        }
    }
    
    @IBAction func btn8(_ sender: UIButton) {
        if(!btn8.isSelected) {
            btn8.imageView?.image = UIImage(named: "btnON")
            btn8.isSelected = true
            btn7.imageView?.image = UIImage(named: "btnOff")
            btn7.isSelected = false
            btn14.imageView?.image = UIImage(named: "btnOff")
            btn14.isSelected = false
        }
        else {
            btn8.imageView?.image = UIImage(named: "btnOff")
            btn8.isSelected = false
        }
    }
    
    @IBAction func btn14(_ sender: UIButton) {
        if(!btn14.isSelected) {
            btn14.imageView?.image = UIImage(named: "btnON")
            btn14.isSelected = true
            btn8.imageView?.image = UIImage(named: "btnOff")
            btn8.isSelected = false
            btn7.imageView?.image = UIImage(named: "btnOff")
            btn7.isSelected = false
        }
        else {
            btn14.imageView?.image = UIImage(named: "btnOff")
            btn14.isSelected = false
        }
    }
    
    @IBAction func dvirBtn(_ sender: UIButton) {
        if(!dvirbtn.isSelected) {
            dvirbtn.imageView?.image = UIImage(named: "tickdump")
            dvirbtn.isSelected = true
        }
        else {
            dvirbtn.imageView?.image = UIImage(named: "untickdump")
            dvirbtn.isSelected = false
        }
    }
    
}
