//
//  Trailer_Shipping_Doc_ViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class Trailer_Shipping_Doc_ViewController: UIViewController {

    
    @IBOutlet var selectVehicle: UITextField!
    @IBOutlet var selectvehicle: UITextField!
    @IBOutlet var trailer1: UITextField!
    @IBOutlet var trailer2: UITextField!
    @IBOutlet var trailer3: UITextField!
    @IBOutlet var shippingdoc: UITextField!
    
    var list = ["a","b","c"]
    let cancelButton = getBarButton("Cancel")
    let doneButton = getBarButton("Done")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        selectVehicle.isUserInteractionEnabled = true
        trailer1.isUserInteractionEnabled = true
        trailer2.isUserInteractionEnabled = true
        trailer3.isUserInteractionEnabled = true
        shippingdoc.isUserInteractionEnabled = true
    }
  
    @IBAction func selectVehicle(_ sender: UIButton) {
        pickerSelection(txtfld: selectVehicle)
    }
    
    @IBAction func trailer1(_ sender: UIButton) {
        pickerSelection(txtfld: trailer1)
    }
    
    @IBAction func trailer2(_ sender: UIButton) {
        pickerSelection(txtfld: trailer2)
    }
    
    @IBAction func trailer3(_ sender: UIButton) {
        pickerSelection(txtfld: trailer3)
    }
    
    @IBAction func shipping(_ sender: UIButton) {
    }
    
    @IBAction func edit_trailer1(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrailerListViewController") as! TrailerListViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func edit_trailer2(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrailerListViewController") as! TrailerListViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func edit_trailer3(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrailerListViewController") as! TrailerListViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func edit_shipping(_ sender: UIButton) {
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func saveBtn(_ sender: UIBarButtonItem) {
    }
    
    func pickerSelection(txtfld: UITextField) {
        if list.count > 0 {
            let acp = ActionSheetStringPicker(title: "", rows: list, initialSelection: 0, doneBlock: {  picker, value, index in
                txtfld.text = index as? String
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: selectVehicle)
            
            acp!.setCancelButton(cancelButton)
            acp!.setDoneButton(doneButton)
            acp!.show()
        }
    }
    
}
