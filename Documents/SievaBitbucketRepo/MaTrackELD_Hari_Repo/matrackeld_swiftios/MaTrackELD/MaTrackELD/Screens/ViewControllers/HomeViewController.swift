//
//  HomeViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LeftCellDelegate, RightCellDelegate {

    var imageArrayLeft = ["logs_3.png","dvir 2.png","suggestion_menu.png","udp.png","accidenticon.png","help_3.png","switchdrive.jpg"]
    var imageArrayRight = ["inspect.jpg","datatransfer.png","unapproved.png","message_alert.png","sett_3.jpg","udp.png",""]
    var imageNameLeft = ["Logs","DVIR","Suggestions","Unidentified Profile","Accident Form","Help","Switch Co-Drivers"]
    var imageNameRight = ["Roadside Inspection","Data Transfer","Unapproved Dates","Messages","Settings","LogonCo-Driver",""]
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        tableView.rowHeight = 150
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
        
    }
    
    func didPressLeftButton(_ tag: Int, sender: UIButton) {
    }
       
    func didPressRightButton(_ tag: Int, sender: UIButton) {
    }
    

}



//Table view
extension HomeViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.SelectRightCell = self
        cell.SelectLeftCell = self
        cell.rightBtn.tag = indexPath.row
        cell.leftBtn.tag = indexPath.row
        cell.leftImage.image = UIImage(named: "\(imageArrayLeft[indexPath.row])")
        cell.rightImage.image = UIImage(named: "\(imageArrayRight[indexPath.row])")
        cell.leftlbl.text = "\(imageNameLeft[indexPath.row])"
        cell.rightlbl.text = "\(imageNameRight[indexPath.row])"
        cell.rightBtn.addTarget(self, action: #selector(self.rightbuttonClicked), for: .touchUpInside)
        cell.leftBtn.addTarget(self, action: #selector(self.leftbuttonClicked), for: .touchUpInside)
        return cell
        
    }
       
    
    @objc func rightbuttonClicked(_ sender: UIButton) {
        
        if(sender.tag == 0) {
            if(dotinsp_dialog == true) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DOTInspectionViewController") as! DOTInspectionViewController
                let navigationController = UINavigationController(rootViewController: vc)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true, completion: nil)
            }
            else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RoadsideInspectionViewController") as! RoadsideInspectionViewController
                let navigationController = UINavigationController(rootViewController: vc)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true, completion: nil)
            }
        }
        
        if(sender.tag == 1) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataTransferViewController") as! DataTransferViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 2) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UnapprovedDatesViewController") as! UnapprovedDatesViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 3) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 4) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @objc func leftbuttonClicked(_ sender: UIButton) {
        
        if(sender.tag == 0) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogViewController") as! LogViewController
           let navigationController = UINavigationController(rootViewController: vc)
           navigationController.modalPresentationStyle = .fullScreen
           self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 1) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "DVIRViewController") as! DVIRViewController
           let navigationController = UINavigationController(rootViewController: vc)
           navigationController.modalPresentationStyle = .fullScreen
           self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 2) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuggestionsViewController") as! SuggestionsViewController
           let navigationController = UINavigationController(rootViewController: vc)
           navigationController.modalPresentationStyle = .fullScreen
           self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 3) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "UDPViewController") as! UDPViewController
           let navigationController = UINavigationController(rootViewController: vc)
           navigationController.modalPresentationStyle = .fullScreen
           self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 4) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
           let navigationController = UINavigationController(rootViewController: vc)
           navigationController.modalPresentationStyle = .fullScreen
           self.present(navigationController, animated: true, completion: nil)
        }
        if(sender.tag == 5) {
           print("help")
        }
    }
  

}
