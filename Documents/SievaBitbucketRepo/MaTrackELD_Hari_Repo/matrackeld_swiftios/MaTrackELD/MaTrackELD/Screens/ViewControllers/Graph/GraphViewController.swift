//
//  GraphViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/3/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import CoreGraphics

class GraphViewController: UIViewController {

    var current_rectfront1 = CGRect.zero
    var flag4 = 0
    let kkGraphHeight1 = 75
    let kkGraphBottom1 = 0
    let kkGraphTop1 = 117
    let kkStepY1 = 15
    let kkOffsetY1 = 0
    let kkCircleRadius1 = 1
    var data = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

}


//MARK:- Graph Operations
    
    class DrawOnView: UIView {
        
        override func draw(_ rect: CGRect) {
            let context = UIGraphicsGetCurrentContext()
            context?.setStrokeColor(UIColor.red.cgColor)
            
            let screenSize = UIScreen.main.bounds
            let screenWidth = screenSize.width
            let screenHeight = screenSize.height
            print("screenWidth, screenHeight ", screenWidth, screenHeight )
            
            let X1 = (31.75/414) * screenWidth
            print("X1 = ", X1)
            let X2 = screenWidth * 0.108
            print("X2 = ", X2)
            X2_value = 3
            let X3 = X2 + (X2_value * (screenWidth/28))
            print("X3 = ", X3)
            
            print("in DrawOnView")
            
//            offduty_status = true
//            sb_status = true
            
            if(offduty_status == true) {
                context?.setLineWidth(1.0)
                context?.move(to: CGPoint(x: X3, y: 0))
                context?.addLine(to: CGPoint(x: X3, y: 18))
                context?.strokePath()
                context?.setLineWidth(2.0)
                context?.move(to: CGPoint(x: X1, y: 0))
                context?.addLine(to: CGPoint(x: X3, y: 0))
                context?.strokePath()
            }
            if(sb_status == true) {
                context?.setLineWidth(1.0)
                context?.move(to: CGPoint(x: X3, y: 0))
                context?.addLine(to: CGPoint(x: X3, y: 18))
                context?.strokePath()
                context?.setLineWidth(1.0)
                context?.move(to: CGPoint(x: X3, y: 18))
                context?.addLine(to: CGPoint(x: 200, y: 18))
                context?.strokePath()
            }
            if(driving_status == true) {
                context?.setLineWidth(1.0)
                context?.move(to: CGPoint(x: X3, y: 34))
                context?.addLine(to: CGPoint(x: X3, y: 50))
                context?.strokePath()
                context?.setLineWidth(1.0)
                context?.move(to: CGPoint(x: X1, y: 34))
                context?.addLine(to: CGPoint(x: X2, y: 34))
                context?.strokePath()
            }
            if(onduty_status == true) {
                context?.setLineWidth(1.0)
                context?.move(to: CGPoint(x: X1, y: 50))
                context?.addLine(to: CGPoint(x: X2, y: 50))
                context?.strokePath()
            }
            
        }
        
    }

    class DrawOnButton: UIButton {
        override func draw(_ rect: CGRect) {
            let context = UIGraphicsGetCurrentContext()
            context?.setLineWidth(4.0)
            context?.setStrokeColor(UIColor.green.cgColor)
            context?.move (to: CGPoint(x: 0, y: 0))
            context?.addLine (to: CGPoint(x: 40, y: 45))
            context?.strokePath()

            print("in DrawOnButton")
        }
    }

    class DrawOnLabel: UILabel {
        override func draw(_ rect: CGRect) {
            let context = UIGraphicsGetCurrentContext()
            context?.setLineWidth(1.0)
            context?.setStrokeColor(UIColor.red.cgColor)
            context?.move (to: CGPoint(x: 0, y: 0))
            context?.addLine (to: CGPoint(x: 35, y: 45))
            context?.strokePath()

            print("in DrawOnLabel")
        }
    }
    















//
//func drawLineGraphWithContext(with ctx: CGContext?) {
//    var screenRect = UIScreen.main.bounds
//    var screenWidth = screenRect.size.width
//    let kkOffsetX1 = Float((31.75 / 414) * screenWidth)
//    var multiplier = 0
//    var falg = false
//    ctx?.setLineWidth(2.0)
//    var prevx: Float = 0
//    var prevy: Float = 0
//    ctx?.setStrokeColor(UIColor.yellow.cgColor)
//    ctx?.setFillColor(UIColor.red.cgColor)
//    var y_offset = 0
//    ctx?.beginPath()
//    let x1 = data[0].value(forKey: "data")[1] as? String
//
//    if x1 == "ON" {
//        y_origin1 = 5
//        ctx?.move(to: CGPoint(x: CGFloat(kkOffsetX1), y: CGFloat((kkStepY1 as? y_origin1) ?? 0.0)))
//        let x = kkOffsetX1
//        y_offset = Int(23 / 1.6)
//        var y = (Int((kkStepY1 as? y_origin1) ?? 0) + y_offset)
//        let rect = CGRect(x: CGFloat(x - kkCircleRadius1), y: CGFloat(y - kkCircleRadius1), width: 2 * kkCircleRadius1, height: 2 * kkCircleRadius1)
//        ctx?.addEllipse(in: rect)
//        prevy = Float(((y_origin1) * kkStepY1)+y_offset)
//        prevx = kkOffsetX1 + Float(kkCircleRadius1)
//    }
//
//    else if x1 == "D" {
//        y_origin1 = 4
//        y_offset = Int(3/2.5)
//        let x = Int(kkOffsetX1)
//        let y = Int(((y_origin1) * (kkStepY1))+y_offset)
//        ctx?.move(to: CGPoint(x: CGFloat(kkOffsetX1), y: CGFloat((y_origin1) * (kkStepY1))))
//        let rect = CGRect(x: Int(x - kkCircleRadius1), y: Int(y - kkCircleRadius1), width: 2 * kkCircleRadius1, height: 2 * kkCircleRadius1)
//        ctx?.addEllipse(in: rect)
//        prevy=Float(((y_origin1) * kkStepY1)+y_offset);
//        prevx=kkOffsetX1+Float(kkCircleRadius1);
//    }
//
//    else if x1 == "SB" {
//        y_origin1 = 3
//        y_offset = 23/4
//        let x = Int(kkOffsetX1)
//        let y = Int(((y_origin1) * (kkStepY1))+y_offset)
//        ctx?.move(to: CGPoint(x: CGFloat(kkOffsetX1), y: CGFloat((y_origin1) * (kkStepY1))))
//        let rect = CGRect(x: Int(x - kkCircleRadius1), y: Int(y - kkCircleRadius1), width: 2 * kkCircleRadius1, height: 2 * kkCircleRadius1)
//        ctx?.addEllipse(in: rect)
//        prevy=Float(((y_origin1) * kkStepY1)+y_offset);
//        prevx=kkOffsetX1+Float(kkCircleRadius1);
//    }
//
//    else if x1 == "OFF" {
//        y_origin1 = 2
//        y_offset = 0
//        let x = Int(kkOffsetX1)
//        let y = Int((y_origin1) * (kkStepY1))
//        ctx?.move(to: CGPoint(x: CGFloat(kkOffsetX1), y: CGFloat((y_origin1) * (kkStepY1))))
//        let rect = CGRect(x: Int(x - kkCircleRadius1), y: Int(y - kkCircleRadius1), width: 2 * kkCircleRadius1, height: 2 * kkCircleRadius1)
//        ctx?.addEllipse(in: rect)
//        prevy=Float(((y_origin1) * kkStepY1)+y_offset);
//        prevx=kkOffsetX1+Float(kkCircleRadius1);
//    }
//
//    var data_size = data.count
//
//    for i in 0...data_size-1 {
//        flag=false
//
//        CGContextSetLineDash(ctx,0,NULL,0)
//        int y_pos = 0
//        NSArray *time = [[data[i] valueForKey:@"data"][0] componentsSeparatedByString:@":"];
//        NSString *second_part = ""
//        int float_part = (int) [time[1] integerValue]
//
//        if (float_part>=0 && float_part<15) {
//            second_part="00"
//            multiplier=0
//        }
//        else if (float_part >= 15 && float_part < 30)    {
//            second_part = "15"
//            multiplier=1
//
//           }else if (float_part>=30 && float_part<45){
//            second_part="30"
//               multiplier=2
//
//           }else if (float_part>=45){
//            second_part="45"
//               multiplier=3
//
//
//           }
//
//           NSString *starttime = [NSString stringWithFormat:@"%@.%@",time[0],second_part];
//           float x_one = [starttime floatValue];
//           NSString *time_float=[NSString stringWithFormat:@"%@",time[0]];
//           float x_one1 = [time_float floatValue];
//
//           int y=0;
//           NSString *x2 = [data[i] valueForKey:@"data"][1];
//
//           if([x2  isEqual: @"D"]){
//
//               CGContextSetLineDash(ctx, 0, NULL,0);
//               y_pos = 4;
//               y_offset = 23/2.5;
//               y = ((y_pos) * kkStepY1)+y_offset;
//
//           }else  if([x2  isEqual: @"ON"]){
//               CGContextSetLineDash(ctx, 0, NULL,0);
//               y_pos = 5;
//               y_offset = 23/1.6;
//               y = ((y_pos) * kkStepY1)+y_offset;
//                ;
//           }else  if([x2  isEqual: @"SB"]){
//
//               CGContextSetLineDash(ctx, 0, NULL,0);
//               y_pos = 3;
//               y_offset = 23/4;
//               y = ((y_pos) * kkStepY1)+y_offset;
//
//           }else  if([x2  isEqual: @"OFF"]){
//
//
//            CGContextSetLineDash(ctx, 0, NULL,0);
//               y_pos = 2;
//               y = ((y_pos) * kkStepY1);
//           CGContextSetLineDash(ctx, 0, NULL,0);
//           float x = kkOffsetX1+(kkStepX1/4*(x_one1*4+multiplier));
//           NSString *event=[data[i] valueForKey:@"event"]
//           if([event isEqualToString:@"11"]){
//             flag=true;
//           }
//
//           CGContextSaveGState(ctx);
//           [self makeLineLayer:ctx lineFromPointA:CGPointMake(prevx, prevy) toPointB:CGPointMake((x+kkCircleRadius1), y) flag:flag];
//
//           prevx=(x+kkCircleRadius1);
//           prevy=y;
//
//
//       }
//
//
//}
