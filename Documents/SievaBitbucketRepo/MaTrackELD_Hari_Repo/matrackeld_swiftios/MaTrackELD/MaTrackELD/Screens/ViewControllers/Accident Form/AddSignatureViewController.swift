//
//  AddSignatureViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/27/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import SwiftSignatureView

class AddSignatureViewController: UIViewController {

    @IBOutlet var signatureView: SwiftSignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
  
    @IBAction func saveBtnAction(_ sender: UIButton) {
        signatureImage = signatureView.getCroppedSignature()!
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeSignatureViewController") as! ChangeSignatureViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func clearBtnAction(_ sender: UIButton) {
        signatureView.clear()
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeSignatureViewController") as! ChangeSignatureViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    
}
