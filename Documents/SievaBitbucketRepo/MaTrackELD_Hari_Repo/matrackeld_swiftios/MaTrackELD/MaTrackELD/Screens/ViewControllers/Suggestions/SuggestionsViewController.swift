//
//  SuggestionsViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SuggestionsViewController: UIViewController {
    
    @IBOutlet var dateLbl: UILabel!
    
    var cancelButton = getBarButton("Cancel")
    var doneButton = getBarButton("Done")
    var selectedStartDateVal = Date(timeInterval: 0, since: Date())
    var dateString = String()
    var selectDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yyyy"
        let dateString = df.string(from: date)
        dateLbl.text = dateString    
    }
    
    @IBAction func calendar(_ sender: UIBarButtonItem) {
        dateSelection()
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func dateSelection() {
        let datePicker = ActionSheetDatePicker(title: dateString, datePickerMode: UIDatePicker.Mode.date, selectedDate:selectedStartDateVal , doneBlock: {
                   picker, value, index in
                   
                   if let selectedDate = value as? Date {
                       self.selectDate = selectedDate
                       let dateForma = DateFormatter()
                       dateForma.dateFormat = "yyyy/MM/dd"
                       dateForma.locale = Locale(identifier: "en_US_POSIX")
                       self.dateLbl.text = selectedDate.toString(dateFormat: "MM/dd/yyyy")
                       self.selectedStartDateVal = (value as? Date)!
                   }
            
                   print("value = \(value!)")
                   print("index = \(String(describing: index))")
                   print("picker = \(String(describing: picker))")
                   return
        }, cancel: { ActionStringCancelBlock in return }, origin: view?.superview)
               //datePicker?.locale = Locale(identifier: "us")
               datePicker?.locale = Locale(identifier: "en_US_POSIX")
               datePicker?.maximumDate = Date(timeInterval: 0, since: Date())
               datePicker!.setCancelButton(cancelButton)
               datePicker!.setDoneButton(doneButton)
               datePicker?.show()
               
    }
    

}
