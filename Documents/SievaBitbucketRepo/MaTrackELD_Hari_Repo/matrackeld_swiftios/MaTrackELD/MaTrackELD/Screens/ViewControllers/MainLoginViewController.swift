//
//  MainLoginViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class MainLoginViewController: UIViewController {
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var offlineSwitch: UISwitch!
    @IBOutlet var signUpLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        username.delegate = self
        password.delegate = self
        
    }
    
    @IBAction func signInBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func connectBleCicked(_ sender: UIButton) {
        
    }
    
    @IBAction func disconnectBleClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func offlineswitchAction(_ sender: UISwitch) {
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
}
