//
//  RecapViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/21/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import Foundation
import CircleProgressBar

class RecapViewController: UIViewController {

    @IBOutlet var breakProgress: CircleProgressBar!
    @IBOutlet var drivingtimeProgress: CircleProgressBar!
    @IBOutlet var dutytimeshiftProgress: CircleProgressBar!
    @IBOutlet var dutytimecycleProgress: CircleProgressBar!
    @IBOutlet var offdutyProgress: CircleProgressBar!
    
    @IBOutlet var hrperday: UILabel!
    @IBOutlet var cyclestart: UILabel!
    @IBOutlet var shiftstart: UILabel!
    @IBOutlet var lastupdate: UILabel!
    
    @IBOutlet var breaktime: UILabel!
    @IBOutlet var drivingtime: UILabel!
    @IBOutlet var shifttime: UILabel!
    @IBOutlet var cycletime: UILabel!
    @IBOutlet var offtime: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        progressView(progress: breakProgress)
        progressView(progress: drivingtimeProgress)
        progressView(progress: dutytimeshiftProgress)
        progressView(progress: dutytimecycleProgress)
        progressView(progress: offdutyProgress)
    }
   

    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refreshBtnAction(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func cameraBtnaction(_ sender: UIBarButtonItem) {
    }
    
}

//ProgressBar View
extension RecapViewController {
    
    func progressView(progress: CircleProgressBar) {
        progress.progressBarWidth = 15.0
        progress.stopAnimation()
        progress.hintViewBackgroundColor = .white
        
        if(progress == breakProgress) {
            progress.progressBarProgressColor = .green
            progress.setProgress(20, animated: false)
        }
        else if(progress == drivingtimeProgress) {
            progress.progressBarProgressColor = .red
            progress.setProgress(20, animated: false)
        }
        else if(progress == dutytimeshiftProgress) {
            progress.progressBarProgressColor = .blue
            progress.setProgress(20, animated: false)
        }
        else if(progress == dutytimecycleProgress) {
            progress.progressBarProgressColor = .purple
            progress.setProgress(20, animated: false)
        }
        else if(progress == offdutyProgress) {
            progress.progressBarProgressColor = .cyan
            progress.setProgress(20, animated: false)
        }
        else {
            progress.progressBarProgressColor = .magenta
            progress.setProgress(20, animated: false)
        }
        
    }
    
}
