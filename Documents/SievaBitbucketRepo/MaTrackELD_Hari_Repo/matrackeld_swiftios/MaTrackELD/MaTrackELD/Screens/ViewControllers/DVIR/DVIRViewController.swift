//
//  DVIRViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/30/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DVIRViewController: UIViewController {

    
    @IBOutlet var locLbl: UILabel!
    @IBOutlet var vehLbl: UILabel!
    @IBOutlet var trailerLbl: UILabel!
    @IBOutlet var driverImg: UIImageView!
    @IBOutlet var mechanicImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func editAction(_ sender: UIButton) {
    }
    
   
    @IBAction func deleteAction(_ sender: UIButton) {
    }
    
    @IBAction func calendar(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func add(_ sender: UIBarButtonItem) {
    }
    @IBAction func back(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
}
