//
//  LogViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/28/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class LogViewController: UIViewController, UIPopoverControllerDelegate {

    @IBOutlet var loggedUser: UILabel!
    @IBOutlet var displayName: UILabel!
    @IBOutlet var codriverLogin: UILabel!
    @IBOutlet var offlineLogin: UILabel!
    @IBOutlet var vehicleStatus: UILabel!
    @IBOutlet var selectVehicle: UILabel!
    @IBOutlet var engpower: UILabel!
    @IBOutlet var driverstatusImage: UIImageView!
    @IBOutlet var driverstatusLbl: UILabel!
    @IBOutlet var lastupdate: UILabel!
    @IBOutlet var bleStatus: UILabel!
    
    
    @IBOutlet var offSwitch: UISwitch!
    @IBOutlet var onSwitch: UISwitch!
    @IBOutlet var sbSwitch: UISwitch!
    @IBOutlet var pcSwitch: UISwitch!
    @IBOutlet var ymSwitch: UISwitch!
    @IBOutlet var wwSwitch: UISwitch!
    @IBOutlet var loadSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func bleBtnAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BLEDeviceViewController") as! BLEDeviceViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func hosgraphBtnAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogDetailsViewController") as! LogDetailsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
       
    }
    
    @IBAction func blequickconnectBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func bledisconnectBtnAction(_ sender: UIButton) {
    }
    
    
    //UISwitch Actions
    @IBAction func offdutySwitch(_ sender: UISwitch) {
        if(offSwitch.isOn) {
            offduty_status = true
            
        }
        else {
            offduty_status = false
        }
        
    }
    
    @IBAction func ondutySwitch(_ sender: UISwitch) {
        if(onSwitch.isOn) {
            onduty_status = true
        }
        else {
            onduty_status = false
        }
    }
    
    @IBAction func sbSwitch(_ sender: UISwitch) {
    }
    
    @IBAction func pcSwitch(_ sender: UISwitch) {
    }
    
    @IBAction func ymSwitch(_ sender: UISwitch) {
    }
    
    @IBAction func wwSwitch(_ sender: UISwitch) {
    }
    
    @IBAction func load_Unload_Swwitch(_ sender: UISwitch) {
    }
    
    //UIBarButtonItem Actions
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func recapBtnAction(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecapViewController") as! RecapViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func cameraBtnAction(_ sender: UIBarButtonItem) {
    }
}





