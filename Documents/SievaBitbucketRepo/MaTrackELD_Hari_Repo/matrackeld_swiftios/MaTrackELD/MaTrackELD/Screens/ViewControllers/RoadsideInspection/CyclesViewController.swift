//
//  CyclesViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/30/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class CyclesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CycleCellDelegate {
    
    @IBOutlet var tableView: UITableView!
    var check = true
    var checklist = [Int]()
    var selectedRows:[IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "CycleTableViewCell", bundle: nil), forCellReuseIdentifier: "CycleTableViewCell")
        tableView.rowHeight = 50
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear

    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func didPresscycleConnect(_ tag: Int, sender: UIButton) {
    }

}



//TableView
extension CyclesViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CycleTableViewCell", for: indexPath) as! CycleTableViewCell
        cell.SelectCycleCell = self
        cell.lbl.text = "00/00/0000 00:00:00"
        if selectedRows.contains(indexPath) {
            cell.btn.setImage(UIImage(named:"tickdump"), for: .normal)
        }
        else {
            cell.btn.setImage(UIImage(named:"untickdump"), for: .normal)
        }
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        return cell
        
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        let selectedIndexPath = IndexPath(row: sender.tag, section: 0)
        if self.selectedRows.contains(selectedIndexPath) {
            //Show All Events
            self.selectedRows.remove(at: self.selectedRows.firstIndex(of: selectedIndexPath)!)
        }
        else {
            //Do Not Show All Events
            self.selectedRows.append(selectedIndexPath)
        }
        self.tableView.reloadData()

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section, indexPath.row)
    }
    
    

}
