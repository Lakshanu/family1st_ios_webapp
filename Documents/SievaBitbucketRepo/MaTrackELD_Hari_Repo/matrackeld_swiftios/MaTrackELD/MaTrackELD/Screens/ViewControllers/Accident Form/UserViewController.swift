//
//  UserViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

    @IBOutlet var additionalView: UIView!
    @IBOutlet var user1_firstname: UITextField!
    @IBOutlet var user1_lastname: UITextField!
    @IBOutlet var user2_firstname: UITextField!
    @IBOutlet var user2_lastname: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        additionalView.isHidden = true
        
    }
    
    @IBAction func addAdditionalUser(_ sender: UIButton) {
        additionalView.isHidden = false
    }
    
    
    @IBAction func prevBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func nextBtn(_ sender: UIButton) {
        if(user1_firstname.text?.isEmpty == false && user1_lastname.text?.isEmpty == false) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VINViewController") as! VINViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteBtn(_ sender: UIButton) {
        additionalView.isHidden = true
    }

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }

}
