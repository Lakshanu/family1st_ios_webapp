//
//  EditAccidentFormViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class EditAccidentFormViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
}
