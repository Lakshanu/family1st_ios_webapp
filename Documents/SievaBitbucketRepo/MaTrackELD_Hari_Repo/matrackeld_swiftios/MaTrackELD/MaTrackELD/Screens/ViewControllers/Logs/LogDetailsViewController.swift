//
//  LogDetailsViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/26/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class LogDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, RefreshCellDelegate {
    
    func didSelectedCollectionViewItem(selectedObject: AnyObject) {
        print("testing")
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var logBtn: UIButton!
    @IBOutlet var formBtn: UIButton!
    @IBOutlet var reviewBtn: UIButton!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var approveLbl: UILabel!
    @IBOutlet var signatureview: UIView!
    @IBOutlet var signatureImage: UIImageView!
    
    var logStatus = Bool()
    var formStatus = Bool()
    var reviewStatus = Bool()
    var cellReturn = UITableViewCell()
    var selectedRows:[IndexPath] = []
    
    var cancelButton = getBarButton("Cancel")
    var doneButton = getBarButton("Done")
    var selectedStartDateVal = Date(timeInterval: 0, since: Date())
    var dateString = String()
    var selectDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("status", logStatus)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 1400
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
        
        if (from_formdetails == true) {
            tableView.register(UINib.init(nibName: "FormTableViewCell", bundle: nil), forCellReuseIdentifier: "FormTableViewCell")
        }
        else {
            tableView.register(UINib.init(nibName: "LogTableViewCell", bundle: nil), forCellReuseIdentifier: "LogTableViewCell")
        }
        
        bottomView.isHidden = true
        topView.isHidden = true
        logBtn.borderColor = .orange
        logBtn.setTitleColor(.orange, for: .normal)
        
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yyyy"
        let dateString = df.string(from: date)
        navigationItem.title = dateString
        
        signatureImage.image = signatureImage1
        
        NotificationCenter.default.addObserver(self, selector: #selector(LogDetailsViewController.ChangeCustomTable), name: NSNotification.Name(rawValue: "annotation"), object: nil)
  
    }
    
     @objc func ChangeCustomTable(){
       print("ChangeCustomTable")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AnnotationViewController") as! AnnotationViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }

    
    @IBAction func logBtnAction(_ sender: UIButton) {

        topView.isHidden = true
        bottomView.isHidden = true
        logBtn.borderColor = .orange
        logBtn.setTitleColor(.orange, for: .normal)
        formBtn.borderColor = .systemGreen
        formBtn.setTitleColor(.systemGreen, for: .normal)
        reviewBtn.borderColor = .systemGreen
        reviewBtn.setTitleColor(.systemGreen, for: .normal)
        
        logStatus = true
        formStatus = false
        reviewStatus = false
        
        tableView.register(UINib.init(nibName: "LogTableViewCell", bundle: nil), forCellReuseIdentifier: "LogTableViewCell")
        tableView.rowHeight = 1400
        tableView.reloadData()
    }
    
    @IBAction func formBtnAction(_ sender: UIButton) {
        
        topView.isHidden = true
        bottomView.isHidden = true
        formBtn.borderColor = .orange
        formBtn.setTitleColor(.orange, for: .normal)
        logBtn.borderColor = .systemGreen
        logBtn.setTitleColor(.systemGreen, for: .normal)
        reviewBtn.borderColor = .systemGreen
        reviewBtn.setTitleColor(.systemGreen, for: .normal)
        
        logStatus = false
        formStatus = true
        reviewStatus = false
        
        tableView.register(UINib.init(nibName: "FormTableViewCell", bundle: nil), forCellReuseIdentifier: "FormTableViewCell")
        tableView.rowHeight = 1400
        tableView.reloadData()
    }
    
    @IBAction func reviewBtnAction(_ sender: UIButton) {
        
        topView.isHidden = false
        bottomView.isHidden = false
        reviewBtn.borderColor = .orange
        reviewBtn.setTitleColor(.orange, for: .normal)
        logBtn.borderColor = .systemGreen
        logBtn.setTitleColor(.systemGreen, for: .normal)
        formBtn.borderColor = .systemGreen
        formBtn.setTitleColor(.systemGreen, for: .normal)
        
        logStatus = false
        formStatus = false
        reviewStatus = true
        
        tableView.register(UINib.init(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
        tableView.rowHeight = 1400
        tableView.reloadData()
    }
    
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func calendar(_ sender: UIButton) {
        dateSelection()
    }
    
    @IBAction func cycleBtnAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CyclesViewController") as! CyclesViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func addEditSign(_ sender: UIButton) {
    
    }
    
    @IBAction func approvelog(_ sender: UIButton) {
    
    }
    
    @IBAction func infobtnAction(_ sender: UIButton) {
    
    }
    
    func didPressRefreshButton(_ tag: Int, sender: UIButton) {
    }
    
    func dateSelection() {
        let datePicker = ActionSheetDatePicker(title: dateString, datePickerMode: UIDatePicker.Mode.date, selectedDate:selectedStartDateVal , doneBlock: {
                   picker, value, index in
                   
                   if let selectedDate = value as? Date {
                       self.selectDate = selectedDate
                       let dateForma = DateFormatter()
                       dateForma.dateFormat = "yyyy/MM/dd"
                       dateForma.locale = Locale(identifier: "en_US_POSIX")
                       self.navigationItem.title = selectedDate.toString(dateFormat: "MM/dd/yyyy")
                       //self.startDate.text = selectedDate.toString(dateFormat: "yyyy/MM/dd")
                       self.selectedStartDateVal = (value as? Date)!
                   }
            
                   print("value = \(value!)")
                   print("index = \(String(describing: index))")
                   print("picker = \(String(describing: picker))")
                   return
        }, cancel: { ActionStringCancelBlock in return }, origin: view?.superview)
               //datePicker?.locale = Locale(identifier: "us")
               datePicker?.locale = Locale(identifier: "en_US_POSIX")
               datePicker?.maximumDate = Date(timeInterval: 0, since: Date())
               datePicker!.setCancelButton(cancelButton)
               datePicker!.setDoneButton(doneButton)
               datePicker?.show()
               
    }
    
    func to_GeneralViewController() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Form_General_ViewController") as! Form_General_ViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }

}


extension LogDetailsViewController {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (logStatus == true) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LogTableViewCell", for: indexPath) as! LogTableViewCell
            if selectedRows.contains(indexPath) {
                cell.showalleventsBtn.setImage(UIImage(named:"tickdump"), for: .normal)
            }
            else {
                cell.showalleventsBtn.setImage(UIImage(named:"untickdump"), for: .normal)
            }
            cell.showalleventsBtn.tag = indexPath.row
            cell.showalleventsBtn.addTarget(self, action: #selector(self.showallevents_buttonClicked), for: .touchUpInside)
            
            
            
            cellReturn = cell
        }
            
        else if (formStatus == true) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormTableViewCell", for: indexPath) as! FormTableViewCell
            cell.generalBtn.addTarget(self, action: #selector(self.generalbuttonClicked), for: .touchUpInside)
            cell.carrierBtn.addTarget(self, action: #selector(self.carrierbuttonClicked), for: .touchUpInside)
            cell.otherBtn.addTarget(self, action: #selector(self.otherbuttonClicked), for: .touchUpInside)
            cellReturn = cell
        }
        else if (reviewStatus == true) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
            cellReturn = cell
        }
            
        else {
            if (from_formdetails == true) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FormTableViewCell", for: indexPath) as! FormTableViewCell
                cell.generalBtn.addTarget(self, action: #selector(self.generalbuttonClicked), for: .touchUpInside)
                cell.carrierBtn.addTarget(self, action: #selector(self.carrierbuttonClicked), for: .touchUpInside)
                cell.otherBtn.addTarget(self, action: #selector(self.otherbuttonClicked), for: .touchUpInside)
                from_formdetails = false
                cellReturn = cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LogTableViewCell", for: indexPath) as! LogTableViewCell
                if selectedRows.contains(indexPath) {
                    cell.showalleventsBtn.setImage(UIImage(named:"tickdump"), for: .normal)
                }
                else {
                    cell.showalleventsBtn.setImage(UIImage(named:"untickdump"), for: .normal)
                }
                cell.showalleventsBtn.tag = indexPath.row
                cell.showalleventsBtn.addTarget(self, action: #selector(self.showallevents_buttonClicked), for: .touchUpInside)
                cellReturn = cell
            }
            
        }
        
        return cellReturn
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt")
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogTableViewCell", for: indexPath) as! LogTableViewCell
        cell.showalleventsBtn.imageView?.image = UIImage(named: "tickdump")
    }
     
    @objc func generalbuttonClicked(_ sender: UIButton) {
        from_general = true
        to_GeneralViewController()
    }
    
   @objc func carrierbuttonClicked(_ sender: UIButton) {
        from_carrier = true
        to_GeneralViewController()
    }
    
   @objc func otherbuttonClicked(_ sender: UIButton) {
        from_other = true
        to_GeneralViewController()
    }
    
    @objc func showallevents_buttonClicked(_ sender: UIButton) {
        let selectedIndexPath = IndexPath(row: sender.tag, section: 0)
        if self.selectedRows.contains(selectedIndexPath) {
            //Show All Events
            self.selectedRows.remove(at: self.selectedRows.firstIndex(of: selectedIndexPath)!)
        }
        else {
            //Do Not Show All Events
            self.selectedRows.append(selectedIndexPath)
        }
        self.tableView.reloadData()
    }
}


//Extensions
func getBarButton(_ title : String) -> UIBarButtonItem{
    let customButton =  UIButton.init(type: UIButton.ButtonType.custom)
    customButton.setTitle(title, for: .normal)
    //customButton.roundCorner(5)
    customButton.frame = CGRect.init(x: 0, y: 5, width: 80, height: 32)
    customButton.setTitleColor(.black, for: .normal)
    //customButton.backgroundColor = UIColor.greenColor
    return UIBarButtonItem.init(customView: customButton)
}


