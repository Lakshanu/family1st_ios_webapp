//
//  TimezoneViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class TimezoneViewController: UIViewController {

    @IBOutlet var hometerminal: UITextField!
    @IBOutlet var cycle: UITextField!
    @IBOutlet var cargo: UITextField!
    @IBOutlet var odo: UITextField!
    @IBOutlet var restart: UITextField!
    @IBOutlet var rest: UITextField!
    
    var timezonelist = ["a","b","c"]
    let cancelButton = getBarButton("Cancel")
    let doneButton = getBarButton("Done")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }

    @IBAction func saveBtn(_ sender: UIBarButtonItem) {
    }
    
    
    @IBAction func timezone(_ sender: UIButton) {
        pickerSelection(txtfld: hometerminal)
    }
    
    @IBAction func cycle(_ sender: UIButton) {
        pickerSelection(txtfld: cycle)
    }
    
    @IBAction func cargo(_ sender: UIButton) {
        pickerSelection(txtfld: cargo)
    }
    
    @IBAction func odo(_ sender: UIButton) {
        pickerSelection(txtfld: odo)
    }
    
    func pickerSelection(txtfld: UITextField) {
        if timezonelist.count > 0 {
            let acp = ActionSheetStringPicker(title: "", rows: timezonelist, initialSelection: 0, doneBlock: {  picker, value, index in
                txtfld.text = index as? String
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: hometerminal)
            
            acp!.setCancelButton(cancelButton)
            acp!.setDoneButton(doneButton)
            acp!.show()
        }
    }
    
}
