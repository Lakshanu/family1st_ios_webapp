//
//  SignatureViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/1/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import SwiftSignatureView

class SignatureViewController: UIViewController {

    @IBOutlet var signatureview: SwiftSignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtnAction(_ sender: UIButton) {
        signatureImage1 = signatureview.getCroppedSignature()!
        if(driver_sign == true) {
            signature_driver = signatureview.getCroppedSignature()!
            signStatus = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DVIR_General_ViewController") as! DVIR_General_ViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if(mechanic_sign == true) {
            signature_mechanic = signatureview.getCroppedSignature()!
            signStatus = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DVIR_General_ViewController") as! DVIR_General_ViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func clearBtnAction(_ sender: UIButton) {
        signatureview.clear()
    }
    
    
}
