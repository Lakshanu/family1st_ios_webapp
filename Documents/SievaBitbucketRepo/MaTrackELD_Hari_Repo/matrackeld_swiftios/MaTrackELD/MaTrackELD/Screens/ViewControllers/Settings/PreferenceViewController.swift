//
//  PreferenceViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class PreferenceViewController: UIViewController {

    
    @IBOutlet var testServer: UISwitch!
    @IBOutlet var cycle: UISwitch!
    @IBOutlet var disable: UISwitch!
    @IBOutlet var ble: UISwitch!
    @IBOutlet var auto: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    
    @IBAction func screenOrientation(_ sender: UIButton) {
    }
    
    
    @IBAction func update(_ sender: UIButton) {
    }
    
    
    @IBAction func enableTestServer(_ sender: UISwitch) {
    }
    
    @IBAction func cycle(_ sender: UISwitch) {
    }
    
    @IBAction func disable(_ sender: UISwitch) {
    }
    
    @IBAction func blesimulator(_ sender: UISwitch) {
    }
    
    @IBAction func autoconnect(_ sender: UISwitch) {
    }
    
}
