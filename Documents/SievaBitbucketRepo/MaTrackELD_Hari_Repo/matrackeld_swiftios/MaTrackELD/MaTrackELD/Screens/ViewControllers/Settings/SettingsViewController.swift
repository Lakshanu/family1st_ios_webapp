//
//  SettingsViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/19/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import Foundation

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, cellDelegate {

    var lblArray = ["Driver Details", "Carrier Details", "Timezone and Cycle Rule", "Trailers/Shipping Document No","Get Last 7 Days Data","Change Password", "Preferences", "Clear Old Data", "Logout"]
    var imageArray = ["sett_driver","sett_carrier","sett_timezone","trailer_icon","backup","sett_changepassword","sett_pref","settings_clearolddata","sett_logout"]
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        tableView.rowHeight = 80
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
     
    }
    
    func didPressButton(_ tag: Int, sender: UIButton) {
        print("Button tag", tag)
    }

}


//TableView
extension SettingsViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return lblArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        cell.selectCell = self
        cell.btn.tag = indexPath.row
        cell.imageview.image = UIImage(named: "\(imageArray[indexPath.row])")
        cell.lbl.text = "\(lblArray[indexPath.row])"
        cell.btn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        return cell
        
    }

    @objc func buttonClicked(_ sender: UIButton) {
        if (sender.tag == 0) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverDetailsViewController") as! DriverDetailsViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if (sender.tag == 1) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarrierDetailsViewController") as! CarrierDetailsViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if (sender.tag == 2) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TimezoneViewController") as! TimezoneViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if (sender.tag == 3) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Trailer_Shipping_Doc_ViewController") as! Trailer_Shipping_Doc_ViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if (sender.tag == 4) {
            print("Get last 7 days data")
        }
        if (sender.tag == 5) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if (sender.tag == 6) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreferenceViewController") as! PreferenceViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        if (sender.tag == 7) {
            print("Clear old data")
        }
        if (sender.tag == 8) {
            print("Logout")
        }
        
    }
    
}


