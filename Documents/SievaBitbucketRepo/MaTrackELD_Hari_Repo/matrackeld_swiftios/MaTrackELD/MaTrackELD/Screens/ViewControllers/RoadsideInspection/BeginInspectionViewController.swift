//
//  BeginInspectionViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import SwiftDataTables

class BeginInspectionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var dottableView: UITableView!
    @IBOutlet var eventTableView: UITableView!
    @IBOutlet var offdutyTime: UILabel!
    @IBOutlet var sbTime: UILabel!
    @IBOutlet var drivingTime: UILabel!
    @IBOutlet var ondutyTime: UILabel!
    @IBOutlet var totalTime: UILabel!
    @IBOutlet var prevBtn: UIButton!
    @IBOutlet var nxtBtn: UIButton!
    
    //Recap
    @IBOutlet var date1: UILabel!
    @IBOutlet var date2: UILabel!
    @IBOutlet var date3: UILabel!
    @IBOutlet var date4: UILabel!
    @IBOutlet var date5: UILabel!
    @IBOutlet var date6: UILabel!
    @IBOutlet var date7: UILabel!
    @IBOutlet var hr1: UILabel!
    @IBOutlet var hr2: UILabel!
    @IBOutlet var hr3: UILabel!
    @IBOutlet var hr4: UILabel!
    @IBOutlet var hr5: UILabel!
    @IBOutlet var hr6: UILabel!
    @IBOutlet var hr7: UILabel!
    
    var cellReturn = UITableViewCell()
    var dateString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dottableView.delegate = self
        dottableView.dataSource = self
        dottableView.register(UINib.init(nibName: "DOTInspectionTableViewCell", bundle: nil), forCellReuseIdentifier: "DOTInspectionTableViewCell")
        dottableView.rowHeight = 500
        dottableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        dottableView.separatorColor = .clear
        
        eventTableView.delegate = self
        eventTableView.dataSource = self
        eventTableView.register(UINib.init(nibName: "EventsTableViewCell", bundle: nil), forCellReuseIdentifier: "EventsTableViewCell")
        eventTableView.rowHeight = 50
        eventTableView.isScrollEnabled = true
        eventTableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        eventTableView.separatorColor = .clear
        
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yyyy"
        dateString = df.string(from: date)
        navigationItem.title = dateString
        
        nxtBtn.isHidden = true
       
        NotificationCenter.default.addObserver(self, selector: #selector(BeginInspectionViewController.ChangeCustomTable), name: NSNotification.Name(rawValue: "annotation_details"), object: nil)
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cycleBtnAction(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CyclesViewController") as! CyclesViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func prevDate(_ sender: UIButton) {
        
    }
    
    @IBAction func nextDate(_ sender: UIButton) {
    
    }
    
    @IBAction func certificateHistory(_ sender: UIButton) {
    
    }
    
 
    @objc func ChangeCustomTable(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AnnotationViewController") as! AnnotationViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }

    
}


//TableView
extension BeginInspectionViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        
        var count = Int()
        if (tableView == tableView) {
            count = 1
        }

        if (tableView == eventTableView) {
            count = 1
        }
       
        return count
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == dottableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DOTInspectionTableViewCell", for: indexPath) as! DOTInspectionTableViewCell
            cellReturn = cell
        }
        
        if (tableView == eventTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell", for: indexPath) as! EventsTableViewCell
            cellReturn = cell
        }
        
        return cellReturn
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = CGFloat()
        if (tableView == tableView) {
            height = 500.0
        }

        if (tableView == eventTableView) {
            height = 150.0
        }
        
        return height
    }

}



