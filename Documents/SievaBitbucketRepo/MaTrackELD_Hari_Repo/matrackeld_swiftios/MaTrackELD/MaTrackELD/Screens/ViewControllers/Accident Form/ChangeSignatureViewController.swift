//
//  ChangeSignatureViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/27/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class ChangeSignatureViewController: UIViewController {

    @IBOutlet var imageview: UIImageView!
    @IBOutlet var imageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imageview.image = signatureImage
    }
    
    @IBAction func prevBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func nextBtn(_ sender: UIButton) {
        //Agreement
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func changeSignature(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddSignatureViewController") as! AddSignatureViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
}
