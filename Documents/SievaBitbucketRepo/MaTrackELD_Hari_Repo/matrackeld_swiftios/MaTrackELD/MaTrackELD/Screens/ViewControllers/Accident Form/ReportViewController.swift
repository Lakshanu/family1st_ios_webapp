//
//  ReportViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {

    @IBOutlet var reportName: UITextField!
    @IBOutlet var loss: UIButton!
    @IBOutlet var work_veh: UIButton!
    @IBOutlet var work_acc: UIButton!
    @IBOutlet var firstaid: UIButton!
    @IBOutlet var call: UIButton!
    @IBOutlet var observ: UIButton!
    
    var lossstate = Bool()
    var workvehstate = Bool()
    var workaccstate = Bool()
    var firstaidstate = Bool()
    var callstate = Bool()
    var observstate = Bool()
   
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func lossBtn(_ sender: UIButton) {
        if (lossstate == false) {
            loss.setImage(UIImage(named: "tickdump"), for: .normal)
            lossstate = true
        }
        else {
            loss.setImage(UIImage(named: "untickdump"), for: .normal)
            lossstate = false
        }
    }
    
    @IBAction func workvehBtn(_ sender: UIButton) {
        if (workvehstate == false) {
            work_veh.setImage(UIImage(named: "tickdump"), for: .normal)
            workvehstate = true
        }
        else {
            work_veh.setImage(UIImage(named: "untickdump"), for: .normal)
            workvehstate = false
        }
    }
    
    @IBAction func workaccBtn(_ sender: UIButton) {
        if (workaccstate == false) {
            work_acc.setImage(UIImage(named: "tickdump"), for: .normal)
            workaccstate = true
        }
        else {
            work_acc.setImage(UIImage(named: "untickdump"), for: .normal)
            workaccstate = false
        }
    }
    
    @IBAction func firstBtn(_ sender: UIButton) {
        if (firstaidstate == false) {
            firstaid.setImage(UIImage(named: "tickdump"), for: .normal)
            firstaidstate = true
        }
        else {
            firstaid.setImage(UIImage(named: "untickdump"), for: .normal)
            firstaidstate = false
        }
    }
    
    @IBAction func callBtn(_ sender: UIButton) {
        if (callstate == false) {
            call.setImage(UIImage(named: "tickdump"), for: .normal)
            callstate = true
        }
        else {
            call.setImage(UIImage(named: "untickdump"), for: .normal)
            callstate = false
        }
    }
    
    @IBAction func observBtn(_ sender: UIButton) {
        if (observstate == false) {
            observ.setImage(UIImage(named: "tickdump"), for: .normal)
            observstate = true
        }
        else {
            observ.setImage(UIImage(named: "untickdump"), for: .normal)
            observstate = false
        }
    }
    
    @IBAction func prevBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
  
    @IBAction func nextBtn(_ sender: UIButton) {
        if(reportName.text!.isEmpty == false) {
            if(lossstate == true || workvehstate == true || workaccstate == true || firstaidstate == true || callstate == true || observstate == true) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
                let navigationController = UINavigationController(rootViewController: vc)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true, completion: nil)
            }
            else {
                print("fill all fields")
            }
        }
        else {
            print("fill all fields")
        }
    }

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    

}
