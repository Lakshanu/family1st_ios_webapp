//
//  DataTransferViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DataTransferViewController: UIViewController {

    @IBOutlet var emailaddrView: UIView!
    @IBOutlet var commentTxt: UITextView!
    @IBOutlet var webBtn: UIButton!
    @IBOutlet var emailBtn: UIButton!
    @IBOutlet var emailTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTxt.delegate = self
        emailaddrView.isHidden = true


    }
    
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func send(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func webBtnAction(_ sender: UIButton) {
        if(!webBtn.isSelected) {
            webBtn.imageView?.image = UIImage(named: "btnON")
            webBtn.isSelected = true
            emailBtn.imageView?.image = UIImage(named: "btnOff")
            emailBtn.isSelected = false
            emailaddrView.isHidden = true
        }
        else {
            webBtn.imageView?.image = UIImage(named: "btnOff")
            webBtn.isSelected = false
        }
    }
    
    @IBAction func emailBtnAction(_ sender: UIButton) {
        if(!emailBtn.isSelected) {
            emailBtn.imageView?.image = UIImage(named: "btnON")
            emailBtn.isSelected = true
            emailaddrView.isHidden = false
            webBtn.imageView?.image = UIImage(named: "btnOff")
            webBtn.isSelected = false
        }
        else {
            emailBtn.imageView?.image = UIImage(named: "btnOff")
            emailBtn.isSelected = false
            emailaddrView.isHidden = true
        }
    }
    
    
}

