//
//  AnnotationViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/27/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class AnnotationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "AnnotationTableViewCell", bundle: nil), forCellReuseIdentifier: "AnnotationTableViewCell")
        tableView.rowHeight = 1000
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
     
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
}



//TableView
extension AnnotationViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnnotationTableViewCell", for: indexPath) as! AnnotationTableViewCell
        return cell
        
    }

}
