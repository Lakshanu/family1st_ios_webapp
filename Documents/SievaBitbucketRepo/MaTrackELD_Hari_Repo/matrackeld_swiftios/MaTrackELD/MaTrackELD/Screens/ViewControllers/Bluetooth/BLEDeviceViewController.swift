//
//  BLEDeviceViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/28/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class BLEDeviceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, BLECellDelegate {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "BLEDevicesTableViewCell", bundle: nil), forCellReuseIdentifier: "BLEDevicesTableViewCell")
        tableView.rowHeight = 50
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
    }

    @IBAction func stopscanBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func startscanBtnAction(_ sender: UIButton) {
    }
    
    
    @IBAction func disconnectBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func forcedisconnectBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogViewController") as! LogViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func didPressbleConnect(_ tag: Int, sender: UIButton) {
    }
    
}


//TableView
extension BLEDeviceViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BLEDevicesTableViewCell", for: indexPath) as! BLEDevicesTableViewCell
        cell.SelectBLECell = self
        cell.lbl.text = "Device Name"
        cell.btn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        return cell
        
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
    }

}
