//
//  RoadsideInspectionViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/19/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.


import UIKit

class RoadsideInspectionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var lblArray = ["Before road side inspection, verify the logs, record additional On Duty hours if applicable, sign the logs including past dates that were not signed.","In DOT inspection screen, verify vehicle name, distance, hours worked today, and remaining hours in the cycle.","Have a printed copy of Matrack ELD manual and Data transfer instruction in the vehicle.","During Road side inspection show the DOT Inspection screen to the authorized safety officer.","When the authorized safety officer request to email the eRODs, ask the email address of the office to send eRODs. Use the Send Email option in the DOT Inspection screen to send eRODs directly to the safely officer."]

    @IBOutlet var dialogBtn: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var imageView: UIImageView!
    var dialog = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "RoadsideInspectionTableViewCell", bundle: nil), forCellReuseIdentifier: "RoadsideInspectionTableViewCell")
        tableView.rowHeight = 130
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear

    }

    @IBAction func btnAction(_ sender: UIButton) {
        if(dialog == true) {
            imageView.image = UIImage(named: "tickdump")
            dotinsp_dialog = true
            dialog = false
        }
        else {
            imageView.image = UIImage(named: "untickdump")
            dotinsp_dialog = false
            dialog = true
        }
    }

    @IBAction func okBtnAction(_ sender: UIButton) {
    }

    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}

//TableView
extension RoadsideInspectionViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoadsideInspectionTableViewCell", for: indexPath) as! RoadsideInspectionTableViewCell
        cell.lbl.text = lblArray[indexPath.row]
        return cell
        
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row % 2 == 0) {
            cell.backgroundColor = UIColor(hex: "D8DEDE")
        }
        else {
            cell.backgroundColor = .white
        }
    }
}
