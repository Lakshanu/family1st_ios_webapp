//
//  DateTimeViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DateTimeViewController: UIViewController {

    @IBOutlet var dateBtn: UIButton!
    @IBOutlet var timeBtn: UIButton!
    @IBOutlet var timelbl: UILabel!
    @IBOutlet var datelbl: UILabel!
    
    
    var cancelButton = getBarButton("Cancel")
    var doneButton = getBarButton("Done")
    var selectedStartDateVal = Date(timeInterval: 0, since: Date())
    var dateString = String()
    var selectDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func date(_ sender: UIButton) {
        dateSelection()
    }
    
    @IBAction func time(_ sender: UIButton) {
        timeSelection()
    }
    
    @IBAction func prevBtn(_ sender: UIButton) {
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "VINViewController") as! VINViewController
          let navigationController = UINavigationController(rootViewController: vc)
          navigationController.modalPresentationStyle = .fullScreen
          self.present(navigationController, animated: true, completion: nil)
      }
      
      @IBAction func nextBtn(_ sender: UIButton) {
        if(datelbl.text?.isEmpty == false && timelbl.text?.isEmpty == false) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
            let navigationController = UINavigationController(rootViewController: vc)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
          }
          else {
            print("jkjkjkjkjk")
          }
      }
    
      @IBAction func backBtn(_ sender: UIBarButtonItem) {
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
          let navigationController = UINavigationController(rootViewController: vc)
          navigationController.modalPresentationStyle = .fullScreen
          self.present(navigationController, animated: true, completion: nil)
      }
    
    func dateSelection() {
        let datePicker = ActionSheetDatePicker(title: dateString, datePickerMode: UIDatePicker.Mode.date, selectedDate:selectedStartDateVal , doneBlock: {
                   picker, value, index in
                   
                   if let selectedDate = value as? Date {
                       self.selectDate = selectedDate
                       let dateForma = DateFormatter()
                       dateForma.dateFormat = "yyyy/MM/dd"
                       dateForma.locale = Locale(identifier: "en_US_POSIX")
                       self.datelbl.text = selectedDate.toString(dateFormat: "MM/dd/yyyy")
                       //self.dateBtn.setTitle(date, for: .normal)
                       self.selectedStartDateVal = (value as? Date)!
                   }
            
                   print("value = \(value!)")
                   print("index = \(String(describing: index))")
                   print("picker = \(String(describing: picker))")
                   return
        }, cancel: { ActionStringCancelBlock in return }, origin: view?.superview)
               //datePicker?.locale = Locale(identifier: "us")
               datePicker?.locale = Locale(identifier: "en_US_POSIX")
               datePicker?.maximumDate = Date(timeInterval: 0, since: Date())
               datePicker!.setCancelButton(cancelButton)
               datePicker!.setDoneButton(doneButton)
               datePicker?.show()
    }

    func timeSelection() {
        let datePicker = ActionSheetDatePicker(title: dateString, datePickerMode: UIDatePicker.Mode.time, selectedDate:selectedStartDateVal , doneBlock: {
                   picker, value, index in
                   
                   if let selectedDate = value as? Date {
                       self.selectDate = selectedDate
                       let dateForma = DateFormatter()
                       dateForma.dateFormat = "hh:mm a"
                       dateForma.locale = Locale(identifier: "en_US_POSIX")
                       self.timelbl.text = selectedDate.toString(dateFormat: "hh:mm a")
                       //self.timeBtn.setTitle(time, for: .normal)
                       self.selectedStartDateVal = (value as? Date)!
                   }
            
                   print("value = \(value!)")
                   print("index = \(String(describing: index))")
                   print("picker = \(String(describing: picker))")
                   return
        }, cancel: { ActionStringCancelBlock in return }, origin: view?.superview)
               //datePicker?.locale = Locale(identifier: "us")
               datePicker?.locale = Locale(identifier: "en_US_POSIX")
               datePicker?.maximumDate = Date(timeInterval: 0, since: Date())
               datePicker!.setCancelButton(cancelButton)
               datePicker!.setDoneButton(doneButton)
               datePicker?.show()
    }

    
}
