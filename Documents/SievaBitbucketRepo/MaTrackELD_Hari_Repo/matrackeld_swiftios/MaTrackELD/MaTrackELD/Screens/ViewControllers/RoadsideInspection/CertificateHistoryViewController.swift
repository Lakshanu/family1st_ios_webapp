//
//  CertificateHistoryViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/30/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class CertificateHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "CertificatehistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CertificatehistoryTableViewCell")
        tableView.rowHeight = 1000
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
    }
    

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
}


//TableView
extension CertificateHistoryViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CertificatehistoryTableViewCell", for: indexPath) as! CertificatehistoryTableViewCell
        return cell
        
    }

}
