//
//  DVIR_General_ViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DVIR_General_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, signCellDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var generalBtn: UIButton!
    @IBOutlet var vehicleBtn: UIButton!
    @IBOutlet var signBtn: UIButton!
    
    var cellReturn = UITableViewCell()
    let timePicker = UIDatePicker()
    let pickerView = UIView()
    
    var cancelButton = getBarButton("Cancel")
    var doneButton = getBarButton("Done")
    var selectedStartDateVal = Date(timeInterval: 0, since: Date())
    var dateString = String()
    var selectDate: Date?
    var selectedRows:[IndexPath] = []
    
    var cond = Bool()
    var defect = Bool()
    var defectneed = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "DVIR_General_TableViewCell", bundle: nil), forCellReuseIdentifier: "DVIR_General_TableViewCell")
        tableView.rowHeight = 400
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
        
        navigationItem.title = "General"
        generalBtn.borderColor = .orange
        generalBtn.setTitleColor(.orange, for: .normal)
        
    }
    
    @IBAction func generalBtnAction(_ sender: UIButton) {
        generalBtn.borderColor = .orange
        generalBtn.setTitleColor(.orange, for: .normal)
        vehicleBtn.borderColor = .systemGreen
        vehicleBtn.setTitleColor(.systemGreen, for: .normal)
        signBtn.borderColor = .systemGreen
        signBtn.setTitleColor(.systemGreen, for: .normal)
        
        tableView.rowHeight = 400
        tableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("viewWillAppear ")
        generalStatus = true
        
        if(signStatus == true) {
            print("signStatus is true")
            tableView.register(UINib.init(nibName: "DVIR_Sign_TableViewCell", bundle: nil), forCellReuseIdentifier: "DVIR_Sign_TableViewCell")
            tableView.rowHeight = 725
            generalStatus = false
            navigationItem.title = "Sign"
            signBtn.borderColor = .orange
            signBtn.setTitleColor(.orange, for: .normal)
            generalBtn.borderColor = .green
            generalBtn.setTitleColor(.green, for: .normal)
            vehicleBtn.borderColor = .green
            vehicleBtn.setTitleColor(.green, for: .normal)
        }
        else {
            tableView.register(UINib.init(nibName: "DVIR_General_TableViewCell", bundle: nil), forCellReuseIdentifier: "DVIR_General_TableViewCell")
            tableView.rowHeight = 400
            signBtn.borderColor = .green
            signBtn.setTitleColor(.green, for: .normal)
            generalBtn.borderColor = .orange
            generalBtn.setTitleColor(.orange, for: .normal)
            vehicleBtn.borderColor = .green
            vehicleBtn.setTitleColor(.green, for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("viewDidAppear ")
        
        
    }
    
    @IBAction func vehicleBtnAction(_ sender: UIButton) {
        navigationItem.title = "Vehicle"
        vehicleBtn.borderColor = .orange
        vehicleBtn.setTitleColor(.orange, for: .normal)
        generalBtn.borderColor = .systemGreen
        generalBtn.setTitleColor(.systemGreen, for: .normal)
        signBtn.borderColor = .systemGreen
        signBtn.setTitleColor(.systemGreen, for: .normal)
        
        generalStatus = false
        vehicleStatus = true
        signStatus = false
        
        tableView.register(UINib.init(nibName: "DVIR_Vehicle_TableViewCell", bundle: nil), forCellReuseIdentifier: "DVIR_Vehicle_TableViewCell")
        tableView.rowHeight = 370
        tableView.reloadData()
    }
    
    @IBAction func signBtnAction(_ sender: UIButton) {
        navigationItem.title = "Sign"
        signBtn.borderColor = .orange
        signBtn.setTitleColor(.orange, for: .normal)
        generalBtn.borderColor = .systemGreen
        generalBtn.setTitleColor(.systemGreen, for: .normal)
        vehicleBtn.borderColor = .systemGreen
        vehicleBtn.setTitleColor(.systemGreen, for: .normal)
        
        generalStatus = false
        vehicleStatus = false
        signStatus = true
        
        tableView.register(UINib.init(nibName: "DVIR_Sign_TableViewCell", bundle: nil), forCellReuseIdentifier: "DVIR_Sign_TableViewCell")
        tableView.rowHeight = 725
        tableView.reloadData()
    }

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        //dismiss(animated: true, completion: nil)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DVIRViewController") as! DVIRViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func didPresscondition(_ tag: Int, sender: UIButton) {
    }
       
    func didPressdefects(_ tag: Int, sender: UIButton) {
    }
       
    func didPressdefectsneed(_ tag: Int, sender: UIButton) {
    }
    
}

//TableView
extension DVIR_General_ViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (generalStatus == true) {
            print("generalStatus == true")
            let cell = tableView.dequeueReusableCell(withIdentifier: "DVIR_General_TableViewCell", for: indexPath) as! DVIR_General_TableViewCell
            cell.odoTxt.keyboardType = .numberPad
            cell.timeTxt.text = DVIR_time
            cell.timeBtn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
            cellReturn = cell
        }
            
        else if (vehicleStatus == true) {
            print("vehicleStatus == true")
            let cell = tableView.dequeueReusableCell(withIdentifier: "DVIR_Vehicle_TableViewCell", for: indexPath) as! DVIR_Vehicle_TableViewCell
            
            if selectedRows.contains(indexPath) {
                cell.preBtn.setImage(UIImage(named:"btnOff"), for: .normal)
                cell.postBtn.setImage(UIImage(named:"btnON"), for: .normal)
            }
            else {
                cell.preBtn.setImage(UIImage(named:"btnON"), for: .normal)
                cell.postBtn.setImage(UIImage(named:"btnOff"), for: .normal)
            }
            cell.preBtn.tag = indexPath.row
            cell.preBtn.addTarget(self, action: #selector(self.pre_buttonClicked), for: .touchUpInside)
            cell.postBtn.tag = indexPath.row
            cell.postBtn.addTarget(self, action: #selector(self.post_buttonClicked), for: .touchUpInside)
            cell.add_remove_veh_defects.tag = indexPath.row
            cell.add_remove_tr_defects.tag = indexPath.row
            cell.add_remove_veh_defects.addTarget(self, action: #selector(self.VehicleDefects), for: .touchUpInside)
            cell.add_remove_tr_defects.addTarget(self, action: #selector(self.TrailerDefects), for: .touchUpInside)
            
            cellReturn = cell
        }
            
        else if (signStatus == true) {
            print("signStatus == true")
            let cell = tableView.dequeueReusableCell(withIdentifier: "DVIR_Sign_TableViewCell", for: indexPath) as! DVIR_Sign_TableViewCell
            cell.selectcell = self
            cell.conditionofvehicle_Btn.tag = 1
            cell.defectscorrected_Btn.tag = 2
            cell.defectsneedtocorrect_Btn.tag = 3
            
            if(cond == true) {
                cell.conditionofvehicle_Btn.setImage(UIImage(named: "btnON"), for: .normal)
                cell.defectscorrected_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
                cell.defectsneedtocorrect_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
                cond = false
            }
            else if(defect == true) {
                cell.conditionofvehicle_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
                cell.defectscorrected_Btn.setImage(UIImage(named: "btnON"), for: .normal)
                cell.defectsneedtocorrect_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
                defect = false
            }
            else if(defectneed == true) {
                cell.conditionofvehicle_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
                cell.defectscorrected_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
                cell.defectsneedtocorrect_Btn.setImage(UIImage(named: "btnON"), for: .normal)
                defectneed = false
            }
            else {
                cell.conditionofvehicle_Btn.setImage(UIImage(named: "btnON"), for: .normal)
                cell.defectscorrected_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
                cell.defectsneedtocorrect_Btn.setImage(UIImage(named: "btnOff"), for: .normal)
            }
            
            cell.conditionofvehicle_Btn.addTarget(self, action: #selector(self.condition_buttonClicked), for: .touchUpInside)
            cell.defectscorrected_Btn.addTarget(self, action: #selector(self.defectcorrect_buttonClicked), for: .touchUpInside)
            cell.defectsneedtocorrect_Btn.addTarget(self, action: #selector(self.defectneed_buttonClicked), for: .touchUpInside)
            cell.driversign_Btn.addTarget(self, action: #selector(self.driver_buttonClicked), for: .touchUpInside)
            cell.mechanicsign_Btn.addTarget(self, action: #selector(self.mechanic_buttonClicked), for: .touchUpInside)
            
            if(driver_sign == true) {
                cell.mechanicSign.image = signature_mechanic
                cell.driverSign.image = signature_driver
                driver_sign = false
            }
            if(mechanic_sign == true) {
                cell.mechanicSign.image = signature_mechanic
                cell.driverSign.image = signature_driver
                mechanic_sign = false
            }
            signStatus = false
            cellReturn = cell
        }
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DVIR_General_TableViewCell", for: indexPath) as! DVIR_General_TableViewCell
            cell.odoTxt.keyboardType = .numberPad
            cell.timeTxt.text = DVIR_time
            cell.timeBtn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
            cellReturn = cell
        }
        
        return cellReturn
        
    }

    @objc func buttonClicked(_ sender: UIButton) {
        
        let datePicker = ActionSheetDatePicker(title: dateString, datePickerMode: UIDatePicker.Mode.time, selectedDate:selectedStartDateVal , doneBlock: {
                   picker, value, index in
                   
                   if let selectedDate = value as? Date {
                       self.selectDate = selectedDate
                       let dateForma = DateFormatter()
                       dateForma.dateFormat = "hh:mm a"
                       dateForma.locale = Locale(identifier: "en_US_POSIX")
                       DVIR_time = selectedDate.toString(dateFormat: "hh:mm a")
                       self.selectedStartDateVal = (value as? Date)!
                   }
                   print("DVIR_time", DVIR_time)
                   print("value = \(value!)")
                   print("index = \(String(describing: index))")
                   print("picker = \(String(describing: picker))")
                   self.tableView.reloadData()
                   return
        }, cancel: { ActionStringCancelBlock in return }, origin: view?.superview)
               //datePicker?.locale = Locale(identifier: "us")
               datePicker?.locale = Locale(identifier: "en_US_POSIX")
               datePicker?.maximumDate = Date(timeInterval: 0, since: Date())
               datePicker!.setCancelButton(cancelButton)
               datePicker!.setDoneButton(doneButton)
               datePicker?.show()
       
        
    }
    
    @objc func pre_buttonClicked(_ sender: UIButton) {
        let selectedIndexPath = IndexPath(row: sender.tag, section: 0)
        if self.selectedRows.contains(selectedIndexPath) {
            //Show All Events
            self.selectedRows.remove(at: self.selectedRows.firstIndex(of: selectedIndexPath)!)
        }
        else {
            //Do Not Show All Events
            self.selectedRows.append(selectedIndexPath)
        }
        self.tableView.reloadData()
    }
    
    @objc func post_buttonClicked(_ sender: UIButton) {
        let selectedIndexPath = IndexPath(row: sender.tag, section: 0)
        if self.selectedRows.contains(selectedIndexPath) {
            //Show All Events
            self.selectedRows.remove(at: self.selectedRows.firstIndex(of: selectedIndexPath)!)
        }
        else {
            //Do Not Show All Events
            self.selectedRows.append(selectedIndexPath)
        }
        self.tableView.reloadData()
    }
    
    @objc func VehicleDefects(_ sender: UIButton) {
        print("vr")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDefectsViewController") as! VehicleDefectsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func TrailerDefects(_ sender: UIButton) {
        print("tr")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrailerDefectViewController") as! TrailerDefectViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func condition_buttonClicked(_ sender: UIButton) {
        cond = true
        tableView.reloadData()
    }
    
    @objc func defectcorrect_buttonClicked(_ sender: UIButton) {
        defect = true
        tableView.reloadData()
    }
    
    @objc func defectneed_buttonClicked(_ sender: UIButton) {
        defectneed = true
        tableView.reloadData()
    }
    
    @objc func driver_buttonClicked(_ sender: UIButton) {
        driver_sign = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func mechanic_buttonClicked(_ sender: UIButton) {
        mechanic_sign = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
        
}
