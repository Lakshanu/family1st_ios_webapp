//
//  UnapprovedDatesViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/25/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class UnapprovedDatesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "UnapprovedDatesTableViewCell", bundle: nil), forCellReuseIdentifier: "UnapprovedDatesTableViewCell")
        tableView.rowHeight = 50
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
               
    }

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}


//TableView
extension UnapprovedDatesViewController {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UnapprovedDatesTableViewCell", for: indexPath) as! UnapprovedDatesTableViewCell
        return cell
        
    }

}

    
