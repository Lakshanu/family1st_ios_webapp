//
//  DetailsViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet var describeTxt: UITextView!
    @IBOutlet var damage_yes: UIButton!
    @IBOutlet var damage_no: UIButton!
    @IBOutlet var incident_yes: UIButton!
    @IBOutlet var incident_no: UIButton!
    
    var damagestatus_yes = Bool()
    var damagestatus_no = Bool()
    var incidentstatus_yes = Bool()
    var incidentstatus_no = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentFormViewController") as! AccidentFormViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func prevBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func nextBtn(_ sender: UIButton) {
        if (describeTxt.text.isEmpty == false ) {
            if (damagestatus_yes == true || damagestatus_no == true) {
                if (incidentstatus_yes == true || incidentstatus_no == true) {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeSignatureViewController") as! ChangeSignatureViewController
                    let navigationController = UINavigationController(rootViewController: vc)
                    navigationController.modalPresentationStyle = .fullScreen
                    self.present(navigationController, animated: true, completion: nil)
                }
                else {
                    
                }
            }
            else {
                
            }
        }
        else {
            
        }
    }
    
    @IBAction func damageyes(_ sender: UIButton) {
        if(damagestatus_yes == true) {
            damage_yes.setImage(UIImage(named: "btnOff"), for: .normal)
            damage_no.setImage(UIImage(named: "btnON"), for: .normal)
            damagestatus_yes = false
            damagestatus_no = true
        }
        else {
            damage_yes.setImage(UIImage(named: "btnON"), for: .normal)
            damage_no.setImage(UIImage(named: "btnOff"), for: .normal)
            damagestatus_yes = true
            damagestatus_no = false
        }
    }
    
    @IBAction func damageno(_ sender: UIButton) {
        if(damagestatus_no == true) {
            damage_no.setImage(UIImage(named: "btnOff"), for: .normal)
            damage_yes.setImage(UIImage(named: "btnON"), for: .normal)
            damagestatus_no = false
            damagestatus_yes = true
        }
        else {
            damage_no.setImage(UIImage(named: "btnON"), for: .normal)
            damage_yes.setImage(UIImage(named: "btnOff"), for: .normal)
            damagestatus_no = true
            damagestatus_yes = false
        }
    }
    
    @IBAction func incidentyes(_ sender: UIButton) {
        if(incidentstatus_yes == true) {
            incident_yes.setImage(UIImage(named: "btnOff"), for: .normal)
            incident_no.setImage(UIImage(named: "btnON"), for: .normal)
            incidentstatus_yes = false
            incidentstatus_no = true
        }
        else {
            incident_yes.setImage(UIImage(named: "btnON"), for: .normal)
            incident_no.setImage(UIImage(named: "btnOff"), for: .normal)
            incidentstatus_yes = true
            incidentstatus_no = false
        }
    }
    
    @IBAction func incidentno(_ sender: UIButton) {
        if(incidentstatus_no == true) {
            incident_no.setImage(UIImage(named: "btnOff"), for: .normal)
            incident_yes.setImage(UIImage(named: "btnON"), for: .normal)
            incidentstatus_no = false
            incidentstatus_yes = true
        }
        else {
            incident_no.setImage(UIImage(named: "btnON"), for: .normal)
            incident_yes.setImage(UIImage(named: "btnOff"), for: .normal)
            incidentstatus_no = true
            incidentstatus_yes = false
        }
    }
    

}
