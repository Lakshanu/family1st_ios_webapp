//
//  VehicleDefectsViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class VehicleDefectsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var defectList = ["Air Compressor","Air Lines","Battery","Belts and Hoses","Body","Brake Accessories","Brakes,Parking","Brakes,Service","Coupling Devices","Defroster/Heater","Drive Line","Engine","Exhaust","Fifth Wheel","Fluid Levels","Frame and Assembly","Front Axle","Fuel Tanks","Horn","Lights","Muffler","Oil Pressure","Radiator","Rear End","Reflectors","Safety Equipment","Starter","Steering","Tire Chains","Tires","Transmission","Trip Recorder","Wheels and Rims","Windows","Windshield Wipers","Truck Other"]
    
    @IBOutlet var tableView: UITableView!
    var selectedRows:[IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "VehicleDefectsTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleDefectsTableViewCell")
        tableView.rowHeight = 40
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
       
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    

}


//TableView
extension VehicleDefectsViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return defectList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleDefectsTableViewCell", for: indexPath) as! VehicleDefectsTableViewCell
        cell.lblNo.text = "\(indexPath.row + 1) - "
        cell.defectsLbl.text = defectList[indexPath.row]
       
        if selectedRows.contains(indexPath) {
            cell.tickBtn.setImage(UIImage(named:"check"), for: .normal)
        }
        else {
            cell.tickBtn.setImage(UIImage(named:"uncheck"), for: .normal)
        }
        cell.tickBtn.tag = indexPath.row
        cell.tickBtn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        return cell
        
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        let selectedIndexPath = IndexPath(row: sender.tag, section: 0)
        if self.selectedRows.contains(selectedIndexPath) {
            //Show All Events
            self.selectedRows.remove(at: self.selectedRows.firstIndex(of: selectedIndexPath)!)
        }
        else {
            //Do Not Show All Events
            self.selectedRows.append(selectedIndexPath)
        }
        self.tableView.reloadData()

    }


}
