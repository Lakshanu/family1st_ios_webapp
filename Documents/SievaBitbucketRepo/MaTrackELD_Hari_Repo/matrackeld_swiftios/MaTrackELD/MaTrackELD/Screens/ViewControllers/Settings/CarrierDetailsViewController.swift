//
//  CarrierDetailsViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 9/2/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class CarrierDetailsViewController: UIViewController {

    @IBOutlet var usdot: UITextField!
    @IBOutlet var carriername: UITextField!
    @IBOutlet var street: UITextField!
    @IBOutlet var city: UITextField!
    @IBOutlet var zip: UITextField!
    @IBOutlet var country: UITextField!
    @IBOutlet var state: UITextField!
    @IBOutlet var street1: UITextField!
    @IBOutlet var city1: UITextField!
    @IBOutlet var zip1: UITextField!
    @IBOutlet var country1: UITextField!
    @IBOutlet var state1: UITextField!
    @IBOutlet var sameasabove: UIButton!
    
    @IBOutlet var str_dot: UILabel!
    @IBOutlet var str_dotno: UILabel!
    @IBOutlet var str_carrier: UILabel!
    
    var same = Bool()
    var countryList = ["USA","Canada"]
    let cancelButton = getBarButton("Cancel")
    let doneButton = getBarButton("Done")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func sameasabove(_ sender: UIButton) {
        if(same == false) {
            sameasabove.setImage(UIImage(named: "tickdump"), for: .normal)
            same = true
            street1.text = street.text
            city1.text = city.text
            zip1.text = zip.text
            country1.text = country.text
            state1.text = state.text
        }
        else {
            sameasabove.setImage(UIImage(named: "untickdump"), for: .normal)
            same = false
            street1.text = ""
            city1.text = ""
            zip1.text = ""
            country1.text = ""
            state1.text = ""
        }
    }
    
    @IBAction func fetchCarrierDetails(_ sender: UIButton) {
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func saveBtn(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func countrySelect(_ sender: UIButton) {
        pickerSelection(txtfld: country)
    }
    
    @IBAction func countrySelect1(_ sender: UIButton) {
        pickerSelection(txtfld: country1)
    }
    
    func pickerSelection(txtfld: UITextField) {
        if countryList.count > 0 {
            let acp = ActionSheetStringPicker(title: "", rows: countryList, initialSelection: 0, doneBlock: {  picker, value, index in
                txtfld.text = index as? String
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: country)
            
            acp!.setCancelButton(cancelButton)
            acp!.setDoneButton(doneButton)
            acp!.show()
        }
    }
    
}
