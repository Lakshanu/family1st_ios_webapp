//
//  EventListTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var lbl: UILabel!
    
    var headerList = ["Sr.No.","EventId","Time","Status","Location","Vehicle","Odometer","Eng Hours Inception","Driver Notes","Annotation","Origin"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}


extension EventListTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 11
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionviewcell", for: indexPath)
        
        print("INDEX: ", indexPath.row, indexPath.section)
        
        let label = cell.viewWithTag(123) as! UILabel //refer the label by Tag
        
        if (sections == 0) {
            
            if (indexPath.section == 0) {
                label.text = "Sr.No"
            }
            if (indexPath.section == 1) {
                label.text = "EventId"
            }
            if (indexPath.section == 2) {
                label.text = "Time"
            }
            if (indexPath.section == 3) {
                label.text = "Status"
            }
            if (indexPath.section == 4) {
                label.text = "Location"
            }
            if (indexPath.section == 5) {
                label.text = "Vehicle"
            }
            if (indexPath.section == 6) {
                label.text = "Odometer"
            }
            if (indexPath.section == 7) {
                label.text = "Eng Hours Inception"
            }
            if (indexPath.section == 8) {
                label.text = "Driver Notes"
            }
            if (indexPath.section == 9) {
                label.text = "Annotation"
            }
            if (indexPath.section == 10) {
                label.text = "Origin"
            }
        }
        
        else {
            label.text = ""
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50.0, height: 50.0)
    }
  
}
