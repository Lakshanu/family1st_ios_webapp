//
//  DVIR_General_TableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DVIR_General_TableViewCell: UITableViewCell {

    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var timeTxt: UITextField!
    @IBOutlet var carrierLbl: UILabel!
    @IBOutlet var carrierTxt: UITextField!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var locationTxt: UITextField!
    @IBOutlet var odoLbl: UILabel!
    @IBOutlet var odoTxt: UITextField!
    @IBOutlet var timeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        timeTxt.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
