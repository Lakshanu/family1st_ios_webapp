//
//  VehicleDefectsTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

protocol tickCellDelegate : class {
    func didPressCellButton(_ tag: Int, sender: UIButton)
}

class VehicleDefectsTableViewCell: UITableViewCell {

    @IBOutlet var tickBtn: UIButton!
    @IBOutlet var lblNo: UILabel!
    @IBOutlet var defectsLbl: UILabel!
    @IBOutlet var majorBtn: UIButton!
    @IBOutlet var major_minor: UITextField!
    @IBOutlet var addNotes: UITextField!
    
    var SelectCell : tickCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func tickBtnAction(_ sender: UIButton) {
        SelectCell?.didPressCellButton(sender.tag, sender: sender)
    }
    
    
}
