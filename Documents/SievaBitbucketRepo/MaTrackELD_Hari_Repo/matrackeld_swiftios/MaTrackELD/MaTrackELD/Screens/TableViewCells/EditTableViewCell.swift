//
//  EditTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/29/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class EditTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {

    var headers_Vin = ["Sr.No","Vehicle","Vin (ECM)","Vin (Driver Entered)","Update"]
    var headers_Trailer = ["Sr.No","Vehicle","Trailer (Admin Provided)","Trailer (Driver Entered)","Update"]
    var headers_Shipping = ["Sr.No","Vehicle","Shipping Doc. (Admin Provided)","Shipping Doc. (Driver Entered)","Update"]
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "EditCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EditCollectionViewCell")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension EditTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditCollectionViewCell", for: indexPath) as! EditCollectionViewCell
        
        cell.txt1.isHidden = true
        cell.txt3.isHidden = true
        
        if(indexPath.section == 0) {
            if (from_vin == true) {
                cell.txt2.text = headers_Vin[indexPath.row]
            }
            if (from_trailer == true) {
                cell.txt2.text = headers_Trailer[indexPath.row]
            }
            if (from_shipping == true) {
                cell.txt2.text = headers_Shipping[indexPath.row]
            }
        }
        else {
            if(indexPath.row == 0) {
                cell.txt2.text = "\(indexPath.section)"
            }
            else if (indexPath.row == 3) {
                if(from_trailer == true) {
                    cell.txt1.isHidden = false
                    cell.txt3.isHidden = false
                    cell.txt1.isUserInteractionEnabled = true
                    cell.txt3.isUserInteractionEnabled = true
                    cell.txt1.placeholder = " trailer1"
                    cell.txt2.placeholder = " trailer2"
                    cell.txt3.placeholder = " trailer3"
                    cell.txt1.cornerRadius = 10.0
                    cell.txt1.borderColor = .lightGray
                    cell.txt1.borderWidth = 1.0
                    cell.txt3.cornerRadius = 10.0
                    cell.txt3.borderColor = .lightGray
                    cell.txt3.borderWidth = 1.0
                }
                cell.txt2.cornerRadius = 10.0
                cell.txt2.borderColor = .lightGray
                cell.txt2.borderWidth = 1.0
                cell.txt2.isUserInteractionEnabled = true
            }
            else if(indexPath.row == 4) {
                let button = UIButton()
                button.frame = CGRect(x: 12, y: 5, width: 45, height: 30)
                button.cornerRadius = 10.0
                button.backgroundColor = .red
                button.titleLabel?.font = UIFont(name: "Helvetica", size: 10)
                button.setTitle("Update", for: .normal)
                button.setTitleColor(.black, for: .normal)
                cell.txt2.addSubview(button)
                cell.txt2.text = ""
            }
            else {
                cell.txt2.text = ""
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (indexPath.row == 0) {
            return CGSize(width: 50,height: 50)
        }
        else if (indexPath.row == 1) {
            return CGSize(width: 80,height: 50)
        }
        else if (indexPath.row == 2) {
            return CGSize(width: 180,height: 50)
        }
        else if (indexPath.row == 3) {
            if(from_trailer == true) {
                return CGSize(width: 250,height: 50)
            }
            else {
                return CGSize(width: 180,height: 50)
            }
        }
        else {
            return CGSize(width: 80, height: 50)
        }
    }


    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

  
}
