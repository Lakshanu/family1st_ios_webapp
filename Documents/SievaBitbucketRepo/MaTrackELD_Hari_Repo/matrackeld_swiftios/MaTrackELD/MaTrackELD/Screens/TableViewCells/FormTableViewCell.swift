//
//  FormTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/27/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class FormTableViewCell: UITableViewCell {

    
    @IBOutlet var vehicle: UILabel!
    @IBOutlet var vin: UILabel!
    @IBOutlet var vin_driver: UILabel!
    @IBOutlet var trailer_admin: UILabel!
    @IBOutlet var trailer_driver: UILabel!
    @IBOutlet var distance: UILabel!
    @IBOutlet var shipping_admin: UILabel!
    @IBOutlet var shipping_driver: UILabel!
    @IBOutlet var driver: UILabel!
    @IBOutlet var driverid: UILabel!
    @IBOutlet var driverlicno: UILabel!
    @IBOutlet var driverlicstate: UILabel!
    @IBOutlet var cyclezone: UILabel!
    @IBOutlet var cycletype: UILabel!
    
    @IBOutlet var usdot: UILabel!
    @IBOutlet var carriername: UILabel!
    @IBOutlet var main_address: UILabel!
    @IBOutlet var home_address: UILabel!
    
    @IBOutlet var username: UILabel!
    @IBOutlet var firstname: UILabel!
    @IBOutlet var lastname: UILabel!
    @IBOutlet var origin: UILabel!
    @IBOutlet var destination: UILabel!
    
    
    @IBOutlet var generalBtn: UIButton!
    @IBOutlet var carrierBtn: UIButton!
    @IBOutlet var otherBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
