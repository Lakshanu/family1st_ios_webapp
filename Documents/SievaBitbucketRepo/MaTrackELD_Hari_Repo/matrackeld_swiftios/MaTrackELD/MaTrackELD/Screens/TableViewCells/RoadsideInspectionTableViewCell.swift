//
//  RoadsideInspectionTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/19/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class RoadsideInspectionTableViewCell: UITableViewCell {

    @IBOutlet var viewImage: UIView!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var lbl: UILabel!
    @IBOutlet var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
