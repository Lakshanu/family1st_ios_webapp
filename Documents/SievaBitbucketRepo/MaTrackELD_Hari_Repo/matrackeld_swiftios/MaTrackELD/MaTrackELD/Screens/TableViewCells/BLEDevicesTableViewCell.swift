//
//  BLEDevicesTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/28/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

protocol BLECellDelegate : class {
    func didPressbleConnect(_ tag: Int, sender: UIButton)
}

class BLEDevicesTableViewCell: UITableViewCell {

    @IBOutlet var btn: UIButton!
    @IBOutlet var lbl: UILabel!
    
    var SelectBLECell : BLECellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
    @IBAction func btnAction(_ sender: UIButton) {
        SelectBLECell?.didPressbleConnect(sender.tag, sender: sender)
    }
    
}
