//
//  Form_OtherTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/26/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class Form_OtherTableViewCell: UITableViewCell, UITextFieldDelegate {

    var list = ["a1","b2","c3"]
    
    @IBOutlet var codriver: UITextField!
    @IBOutlet var codriverusr: UITextField!
    @IBOutlet var firstname: UITextField!
    @IBOutlet var lastname: UITextField!
    @IBOutlet var from: UITextField!
    @IBOutlet var to: UITextField!
    @IBOutlet var notes: UITextField!
    
    let cancelButton = getBarButton("Cancel")
    let doneButton = getBarButton("Done")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        codriverusr.delegate = self
        firstname.delegate = self
        lastname.delegate = self
        from.delegate = self
        to.delegate = self
        notes.delegate = self
        
        codriver.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func select_codriver(_ sender: UIButton) {
        pickerSelection(txtfld: codriver)
    }
    
    @IBAction func fetchBtn(_ sender: UIButton) {
    }
    
    
    func pickerSelection(txtfld: UITextField) {
        if list.count > 0 {
            let acp = ActionSheetStringPicker(title: "", rows: list, initialSelection: 0, doneBlock: {  picker, value, index in
                txtfld.text = index as? String
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: codriver)
            
            acp!.setCancelButton(cancelButton)
            acp!.setDoneButton(doneButton)
            acp!.show()
        }
    }
    
}
