//
//  SettingsTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/19/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

protocol cellDelegate : class {
    func didPressButton(_ tag: Int, sender: UIButton)
}

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet var btn: UIButton!
    @IBOutlet var lbl: UILabel!
    @IBOutlet var imageview: UIImageView!
    
    var selectCell : cellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
    @IBAction func btnAction(_ sender: UIButton) {
        selectCell?.didPressButton(sender.tag, sender: sender)
    }
    
}
