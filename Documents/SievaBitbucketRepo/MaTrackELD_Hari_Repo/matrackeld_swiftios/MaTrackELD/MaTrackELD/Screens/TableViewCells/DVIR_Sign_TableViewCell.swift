//
//  DVIR_Sign_TableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

protocol signCellDelegate : class {
    func didPresscondition(_ tag: Int, sender: UIButton)
    func didPressdefects(_ tag: Int, sender: UIButton)
    func didPressdefectsneed(_ tag: Int, sender: UIButton)
}

class DVIR_Sign_TableViewCell: UITableViewCell {

    @IBOutlet var conditionofvehicle_Btn: UIButton!
    @IBOutlet var defectscorrected_Btn: UIButton!
    @IBOutlet var defectsneedtocorrect_Btn: UIButton!
    @IBOutlet var driverSign: UIImageView!
    @IBOutlet var mechanicSign: UIImageView!
    @IBOutlet var driversign_Btn: UIButton!
    @IBOutlet var mechanicsign_Btn: UIButton!
    @IBOutlet var mechanicNotes: UITextField!
    
    var selectcell : signCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
    @IBAction func conditionBtn(_ sender: UIButton) {
        selectcell?.didPresscondition(sender.tag, sender: sender)
    }
    
    @IBAction func defectBtn(_ sender: UIButton) {
        selectcell?.didPressdefects(sender.tag, sender: sender)

    }
    
    @IBAction func defectneedBtn(_ sender: UIButton) {
        selectcell?.didPressdefectsneed(sender.tag, sender: sender)

    }
    
}
