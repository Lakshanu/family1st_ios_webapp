//
//  CycleTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/30/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

protocol CycleCellDelegate : class {
    func didPresscycleConnect(_ tag: Int, sender: UIButton)
}

class CycleTableViewCell: UITableViewCell {

    @IBOutlet var btnimg: UIImageView!
    @IBOutlet var btn: UIButton!
    @IBOutlet var lbl: UILabel!
    
    var SelectCycleCell : CycleCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnclick(_ sender: UIButton) {
        SelectCycleCell?.didPresscycleConnect(sender.tag, sender: sender)
    }
}
