//
//  LogTableCollectionViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/26/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class LogTableCollectionViewCell: UICollectionViewCell {

    @IBOutlet var lblView: UIView!
    @IBOutlet var lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        lblView.borderWidth = 1.0
        lblView.borderColor = .black
    }
    
    

}
