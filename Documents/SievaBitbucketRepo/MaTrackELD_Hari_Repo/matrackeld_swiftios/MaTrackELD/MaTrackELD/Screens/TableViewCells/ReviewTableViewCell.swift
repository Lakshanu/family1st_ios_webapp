//
//  ReviewTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/27/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {

    var headers = ["Sr.No.","Event Id","Time","Status","Location","Vehicle","Odometer","Eng Hours Inception","Driver Notes","Annotation","Origin"]
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var offdutyTime: UILabel!
    @IBOutlet var sbdutyTime: UILabel!
    @IBOutlet var drivingTime: UILabel!
    @IBOutlet var ondutyTime: UILabel!
    @IBOutlet var totalTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "LogTableCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LogTableCollectionViewCell")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}



extension ReviewTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LogTableCollectionViewCell", for: indexPath) as! LogTableCollectionViewCell
        
        print("INDEX: ", indexPath.section, indexPath.row)
        
        if(indexPath.section == 0) {
            cell.lbl.text = headers[indexPath.row]
        }
        else {
            if(indexPath.row == 0) {
                cell.lbl.text = "\(indexPath.section)"
            }
            else if(indexPath.row == 9) {
                let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
                let underlineAttributedString = NSAttributedString(string: "Details", attributes: underlineAttribute)
                cell.lbl.textColor = .blue
                cell.lbl.attributedText = underlineAttributedString
            }
            else {
                cell.lbl.text = ""
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (indexPath.row == 4) {
            return CGSize(width: 60,height: 34)
        }
        else if (indexPath.row == 6) {
            return CGSize(width: 60,height: 34)
        }
        else if (indexPath.row == 7) {
            return CGSize(width: 110,height: 34)
        }
        else if (indexPath.row == 8) {
            return CGSize(width: 80,height: 34)
        }
        else if (indexPath.row == 9) {
            return CGSize(width: 80,height: 34)
        }
        else {
            return CGSize(width: 50, height: 34)
        }
    }


    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

  
}
