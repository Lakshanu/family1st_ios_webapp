//
//  Form_GeneralTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/25/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class Form_GeneralTableViewCell: UITableViewCell, UITextFieldDelegate {

    var vinList = ["a","b","c"]
    
    @IBOutlet var vehicle: UITextField!
    @IBOutlet var vin: UITextField!
    @IBOutlet var vin_driver: UITextField!
    @IBOutlet var trailer_admin: UITextField!
    @IBOutlet var trailer_driver: UITextField!
    @IBOutlet var distance: UITextField!
    @IBOutlet var shipping_admin: UITextField!
    @IBOutlet var shipping_driver: UITextField!
    @IBOutlet var firstname: UITextField!
    @IBOutlet var lastname: UITextField!
    @IBOutlet var driverid: UITextField!
    @IBOutlet var driverlicno: UITextField!
    @IBOutlet var driverlicstate: UITextField!
    @IBOutlet var cyclezone: UITextField!
    @IBOutlet var cargotype: UITextField!
    @IBOutlet var extemptImage: UIImageView!
    
    @IBOutlet var exemptBtn: UIButton!
    let cancelButton = getBarButton("Cancel")
    let doneButton = getBarButton("Done")
    var exempt = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        vehicle.delegate = self
        vin.delegate = self
        vin_driver.delegate = self
        trailer_admin.delegate = self
        trailer_driver.delegate = self
        distance.delegate = self
        shipping_admin.delegate = self
        shipping_driver.delegate = self
        firstname.delegate = self
        lastname.delegate = self
        driverid.delegate = self
        driverlicno.delegate = self
        driverlicstate.delegate = self
        cyclezone.delegate = self
        cargotype.delegate = self
        
        driverlicstate.isUserInteractionEnabled = false
        cyclezone.isUserInteractionEnabled = false
        cargotype.isUserInteractionEnabled = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func fetchBtn(_ sender: UIButton) {
    }
    
    @IBAction func vinBtnAction(_ sender: UIButton) {
        from_vin = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "edit_vin"), object:nil, userInfo: nil)
    }
    
    @IBAction func trailerBtnAction(_ sender: UIButton) {
        from_trailer = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "edit_trailer"), object:nil, userInfo: nil)
    }
    
    @IBAction func shippingbtnAction(_ sender: UIButton) {
        from_shipping = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "edit_shipping"), object:nil, userInfo: nil)
    }
    
    @IBAction func driverlicstate(_ sender: UIButton) {
        pickerSelection(txtfld: driverlicstate)
    }
    
    @IBAction func cyclezone(_ sender: UIButton) {
        pickerSelection(txtfld: cyclezone)
    }
    
    @IBAction func cargotype(_ sender: UIButton) {
        pickerSelection(txtfld: cargotype)
    }
    
    
    @IBAction func exemptBtnAction(_ sender: UIButton) {
        if (exempt == true) {
            extemptImage.image = UIImage(named: "tickdump")
            exempt = false
        }
        else {
            extemptImage.image = UIImage(named: "untickdump")
            exempt = true
        }
        
        
    }
    
    func pickerSelection(txtfld: UITextField) {

        if vinList.count > 0 {
            let acp = ActionSheetStringPicker(title: "", rows: vinList, initialSelection: 0, doneBlock: {  picker, value, index in
                txtfld.text = index as? String
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: vin)
            
            acp!.setCancelButton(cancelButton)
            acp!.setDoneButton(doneButton)
            acp!.show()
        }
    }
    
}
