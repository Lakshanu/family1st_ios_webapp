//
//  EventsTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/25/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {

    var headers = ["Sr.No.","Event Id","Time","Status","Location","Vehicle","Odometer","Eng Hours Inception","Driver Notes","Annotation","Origin"]
    @IBOutlet var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "LogTableCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LogTableCollectionViewCell")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}



extension EventsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LogTableCollectionViewCell", for: indexPath) as! LogTableCollectionViewCell
        print("INDEX: ", indexPath.section, indexPath.row)
        
        if(indexPath.section == 0) {
            cell.lbl.text = headers[indexPath.row]
        }
        else {
            if(indexPath.row == 0) {
                cell.lbl.text = "\(indexPath.section)"
            }
            else if(indexPath.row == 9) {
                let button = UIButton()
                button.frame = CGRect(x: 10, y: 0, width: 50, height: 30)
                let attrs = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13.0), NSAttributedString.Key.foregroundColor : UIColor.blue, NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
                let attributedString = NSMutableAttributedString(string:"")
                let buttonTitleStr = NSMutableAttributedString(string:"Details", attributes:attrs)
                attributedString.append(buttonTitleStr)
                button.setAttributedTitle(attributedString, for: .normal)
                cell.lbl.text = ""
                cell.lbl.addSubview(button)
            }
            else {
                cell.lbl.text = ""
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (indexPath.row == 7) {
            return CGSize(width: 110,height: 44)
        }
        else {
            return CGSize(width: 80, height: 44)
        }
    }


    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row == 9) {
            print("indexpath.section -> ", indexPath.section)
            annotationSection = indexPath.section
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "annotation_details"), object:nil, userInfo: nil)
        }
   }
    
}


