//
//  EditCollectionViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/29/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class EditCollectionViewCell: UICollectionViewCell {

    @IBOutlet var view: UIView!
    @IBOutlet var txt1: UITextField!
    @IBOutlet var txt2: UITextField!
    @IBOutlet var txt3: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        view.borderWidth = 1.0
        view.borderColor = .black
        
        txt1.borderColor = .clear
        txt2.borderColor = .clear
        txt3.borderColor = .clear
        
        txt1.isUserInteractionEnabled = false
        txt2.isUserInteractionEnabled = false
        txt3.isUserInteractionEnabled = false
    }

}
