//
//  Form_CarrierTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/25/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class Form_CarrierTableViewCell: UITableViewCell, UITextFieldDelegate {

    var list = ["a","b","c"]
    
    @IBOutlet var usdot: UITextField!
    @IBOutlet var carriername: UITextField!
    
    @IBOutlet var street: UITextField!
    @IBOutlet var city: UITextField!
    @IBOutlet var zip: UITextField!
    @IBOutlet var country: UITextField!
    @IBOutlet var state: UITextField!
    
    @IBOutlet var street1: UITextField!
    @IBOutlet var city1: UITextField!
    @IBOutlet var zip1: UITextField!
    @IBOutlet var country1: UITextField!
    @IBOutlet var state1: UITextField!
    
    @IBOutlet var fetchCarrier: UIButton!
    @IBOutlet var fetchBtn: UIButton!
    @IBOutlet var sameasaboveBtn: UIButton!
    
    var sameasabove = true
    let cancelButton = getBarButton("Cancel")
    let doneButton = getBarButton("Done")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        usdot.delegate = self
        carriername.delegate = self
        street.delegate = self
        city.delegate = self
        zip.delegate = self
        
        country.isUserInteractionEnabled = false
        country1.isUserInteractionEnabled = false
        state.isUserInteractionEnabled = false
        state1.isUserInteractionEnabled = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func sameasabove(_ sender: UIButton) {
        if (sameasabove == true) {
            sameasaboveBtn.imageView?.image = UIImage(named: "tickdump")
            sameasabove = false
        }
        else {
            sameasaboveBtn.imageView?.image = UIImage(named: "untickdump")
            sameasabove = true
        }
    }
    
    @IBAction func country(_ sender: UIButton) {
        pickerSelection(txtfld: country)
    }
    
    @IBAction func state(_ sender: UIButton) {
        pickerSelection(txtfld: state)
    }
    
    @IBAction func country1(_ sender: UIButton) {
        pickerSelection(txtfld: country1)
    }
    
    @IBAction func state1(_ sender: UIButton) {
        pickerSelection(txtfld: state1)
    }
    
    
    func pickerSelection(txtfld: UITextField) {
           if list.count > 0 {
               let acp = ActionSheetStringPicker(title: "", rows: list, initialSelection: 0, doneBlock: {  picker, value, index in
                   txtfld.text = index as? String
                   return
               }, cancel: { ActionStringCancelBlock in return }, origin: zip)
               
               acp!.setCancelButton(cancelButton)
               acp!.setDoneButton(doneButton)
               acp!.show()
           }
    }
    
}
