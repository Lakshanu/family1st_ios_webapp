//
//  DOTInspectionTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DOTInspectionTableViewCell: UITableViewCell {

    
    @IBOutlet var driverLbl: UILabel!
    @IBOutlet var driverid: UILabel!
    @IBOutlet var license: UILabel!
    @IBOutlet var startendodo: UILabel!
    @IBOutlet var distance: UILabel!
    @IBOutlet var carrier: UILabel!
    @IBOutlet var vehicle: UILabel!
    @IBOutlet var codriveruser: UILabel!
    @IBOutlet var shippingdoc: UILabel!
    @IBOutlet var cyclestart: UILabel!
    @IBOutlet var cargotype: UILabel!
    
    @IBOutlet var cycle: UILabel!
    @IBOutlet var vin: UILabel!
    @IBOutlet var licensestate: UILabel!
    @IBOutlet var startendenghr: UILabel!
    @IBOutlet var timezone: UILabel!
    @IBOutlet var restarthr: UILabel!
    @IBOutlet var trailer: UILabel!
    @IBOutlet var codrivername: UILabel!
    @IBOutlet var hrsavail: UILabel!
    @IBOutlet var hrsworked: UILabel!
    @IBOutlet var address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
