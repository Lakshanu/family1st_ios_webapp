//
//  DataTransferTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

protocol CellDelegate : class {
    func didPressButton(_ tag: Int, sender: UIButton)
}

class DataTransferTableViewCell: UITableViewCell {

    @IBOutlet var view: UIView!
    @IBOutlet var lblTxt: UILabel!
    @IBOutlet var lbl: UILabel!
    @IBOutlet var btn: UIButton!
    
    var SelectCell : CellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func BtnClicked(_ sender: UIButton) {
        SelectCell?.didPressButton(sender.tag, sender: sender)
    }
    
}
