//
//  UDPTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/27/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class UDPTableViewCell: UITableViewCell {

    var headers = ["Sr.No.","Event Id","Time","Status","Location","Vehicle","Odometer","Eng Hours Inception","Driver Notes","Annotation","Origin","Claim","Edit"]
    @IBOutlet var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "LogTableCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LogTableCollectionViewCell")
    }
    
}


extension UDPTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 13
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LogTableCollectionViewCell", for: indexPath) as! LogTableCollectionViewCell
        
        print("INDEX: ", indexPath.section, indexPath.row)
        
        if(indexPath.section == 0) {
            cell.lbl.text = headers[indexPath.row]
        }
        else {
            if(indexPath.row == 0) {
                cell.lbl.text = "\(indexPath.section)"
            }
            else if(indexPath.row == 9) {
                let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
                let underlineAttributedString = NSAttributedString(string: "Details", attributes: underlineAttribute)
                cell.lbl.textColor = .blue
                cell.lbl.attributedText = underlineAttributedString
            }
            else if(indexPath.row == 11) {
                let button = UIButton()
                button.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
                button.cornerRadius = 10.0
                button.backgroundColor = .red
                button.titleLabel?.font = UIFont(name: "Helvetica", size: 10)
                button.setTitle("Claim", for: .normal)
                cell.lbl.addSubview(button)
                let label = UILabel(frame: CGRect(x: 50, y: 0, width: 80, height: 30))
                label.textAlignment = .center
                label.font = UIFont(name: "Helvetica", size: 10)
                label.text = "VehicleName"
                cell.lbl.text = ""
                cell.lbl.addSubview(label)
            }
            else if(indexPath.row == 12) {
                let image: UIImage = UIImage(named: "edit")!
                var bgImage: UIImageView?
                bgImage = UIImageView(image: image)
                bgImage!.frame = CGRect(x: 50,y: 0,width: 20,height: 20)
                cell.lbl.addSubview(bgImage!)
                cell.lbl.text = ""
            }
            else {
                cell.lbl.text = ""
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (indexPath.row == 4) {
            return CGSize(width: 60,height: 45)
        }
        else if (indexPath.row == 6) {
            return CGSize(width: 60,height: 45)
        }
        else if (indexPath.row == 7) {
            return CGSize(width: 110,height: 45)
        }
        else if (indexPath.row == 8) {
            return CGSize(width: 80,height: 45)
        }
        else if (indexPath.row == 9) {
            return CGSize(width: 80,height: 45)
        }
        else if (indexPath.row == 11) {
            return CGSize(width:150,height: 45)
        }
        else {
            return CGSize(width: 50, height: 45)
        }
    }


    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

  
}
