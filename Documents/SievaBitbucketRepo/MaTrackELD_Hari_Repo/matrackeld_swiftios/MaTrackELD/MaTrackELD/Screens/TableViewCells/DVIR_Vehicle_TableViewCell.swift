//
//  DVIR_Vehicle_TableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/24/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DVIR_Vehicle_TableViewCell: UITableViewCell {

    @IBOutlet var vehicletxt: UITextField!
    @IBOutlet var trailertxt: UITextField!
    @IBOutlet var preBtn: UIButton!
    @IBOutlet var postBtn: UIButton!
    @IBOutlet var add_remove_veh_defects: UIButton!
    @IBOutlet var add_remove_tr_defects: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
