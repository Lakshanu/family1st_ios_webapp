//
//  Global Variables.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/25/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import Foundation
import UIKit

var sections = Int()
var signatureImage = UIImage()
var signatureImage1 = UIImage()
var annotationSection = Int()
var from_formdetails = Bool()
var from_general = Bool()
var from_carrier = Bool()
var from_other = Bool()
var from_vin = Bool()
var from_trailer = Bool()
var from_shipping = Bool()
var dotinsp_dialog = Bool()
var DVIR_time = String()
var driver_sign = Bool()
var mechanic_sign = Bool()
var generalStatus = Bool()
var vehicleStatus = Bool()
var signStatus = Bool()
var signature_driver = UIImage()
var signature_mechanic = UIImage()

//Graph operation
var X2_value = CGFloat()
var Y_value = CGFloat()
var offduty_status = Bool()
var sb_status = Bool()
var driving_status = Bool()
var onduty_status = Bool()
